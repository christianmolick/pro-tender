# pro-tender

The pro-tender service is for tracking production and professional services.

## License
This module is unlicensed public domain open source.
See the UNLICENSE file or unlicense.org for more information.

## Installation and configuration
Get the module with npm install, or clone from repository,
use npm install to populate node_modules folder,
set database access and service port data in configuration file.
Create and populate database with create-db*.sh scripts

## Quick use
Start and test with npm start and then npm test.

## General use
Start with npm start, then use interfaces as described in the documentation.

## Documentation
The docs folder has documents explaining intended usage.

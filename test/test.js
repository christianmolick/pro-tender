// test.js: Execute tests for all pro service functionality.
// This primarily loads modules and triggers tests
// from modules in the test/support folder.

// local modules for configuration, basic 
// service configuration required for access
let config = require('../app/config');
// base has essential values and get/post routines
let base = require('./support/base');
// fetch service properties
let properties = require('./support/properties'); 
let selection = require('./support/selection'); 
// test entities
let person = require('./support/person');
let place = require('./support/place');
let event = require('./support/event'); 

// execute initial tests that require only basic values and routines
properties.test();
selection.test();

// execute tests that require arrangement and completion
person.test();
place.test();
event.test();


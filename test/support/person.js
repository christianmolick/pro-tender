// person.js: Test person related operations.

// require modules for service configuration, base testing
// and arrange and complete to set up and clean up testing context
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var person = {};
// test person reference for operations on test posting
var testPersonRef;

// testInspection function is exported and executes all tests
function testPerson() {
    describe('person: request, inspect, modify, and delete', function() {
        // first set up test context by making account and signing in

        // define variables to store account and sign in permit references
        var testAccountRef;
        var testPermit;

        // request account
        it('person: request account for testing', function(done) {
            let requestAccountArgs = {
                data: {
                    language: 'en',
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                 },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/request', requestAccountArgs, function(data) {                
                testAccountRef = data.ref;
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                done();
            });
        });  // it requests account block

        // sign in: request challenge for newly created test account
        it('person: request challenge to sign in', function(done) {
            // get challenge
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.accountTenderPort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
               // challenge should be base.testChallenge
                done();
            });
        });  // it get challenge block
        
        // post response to challenge to complete account sign in
        it('person: sign in by posting challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });  // it posts challenge response and receives permit block

        // strategy is post, get, delete, get
        // where first get verifies content and second verifies deletion
        // really should check ops with invalid account/permit
        // or badly mangled or malicious input data
        
        // post person
        it('person: post person', function(done) {
            let postArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    name: 'test person',
                    full_name: 'Teste Personne',
                    language: 'en',
                    status: 'test',
                    profile: 'only for testing',
                    preferences: 'deletion upon completion',
                    provides: 'payment on delivery of services'
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/person', postArgs, function(postData) {
                assert.ok(postData.result === 'done');
                testPersonRef = postData.ref;
                done();
            });
        });

        // get posted person
        it('person: fetch returns posted person data', function(done) {
            let getArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    ref: testPersonRef
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/person', getArgs, function(getData) {
                assert.ok(getData.result === 'okay');
                // really need to check returned data fully
                done();
            });
        });

        // delete person
        it('person: delete person', function(done) {
            let deleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    ref: testPersonRef
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/person/delete', deleteArgs, function(deleteData) {
                assert.ok(deleteData.result === 'done');
                done();
            });
        });
        
        // attempt to fetch deleted person

        // all testing done, now clean up environment by deleting account
        // first request account deletion
        it('person: requests test account deletion', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });  // end request delete block
        // complete deletion with confirmation
        it('person: confirms test account deletion', function(done) {
        let confirmDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });  // end confirm delete block

    });  // describe valid and invalid inspection tests
}    // end of entire testInspection test routine block
person.test = testPerson;
module.exports = person;

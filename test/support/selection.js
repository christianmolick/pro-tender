// selection.js: Test selection related operations.
// plan: post, fetch, post update, fetch again, delete, fetch failure

// require modules for service configuration, base testing
// and arrange and complete to set up and clean up testing context
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');

// define object for sharing test routine
var selection = {};
// test selection reference for operations on test posting
var testSelectionRef;
let testContext = '11111111-1111-1111-1111-111111111111';
let testContextType = 'event';
let testField = '22222222-2222-2222-2222-222222222222';
let testFieldType = 'place';
let testFieldName = 'location';

// testInspection function is exported and executes all tests
function testSelection() {
    describe('selection: post, fetch, post update, fetch, delete, fail fetch', function() {
        // first set up test context by making account and signing in

        // define variables to store account and sign in permit references
        var testAccountRef;
        var testPermit;

        // request account
        it('selection: request account for testing', function(done) {
            let requestAccountArgs = {
                data: {
                    language: 'en',
                    credentials: [base.testChallenge, base.testResponse],
                    attributes: [
                        'name', base.testAccountName, 'short',
                        'name', base.testAccountFullName, 'full',
                        'contact', base.testEmail, 'primary email'
                    ]
                 },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/request', requestAccountArgs, function(data) {                
                testAccountRef = data.ref;
                assert.ok(data.result === 'done');
                assert.ok(data.reason === 'account-created');
                done();
            });
        });  // it requests account block

        // sign in: request challenge for newly created test account
        it('selection: request challenge to sign in', function(done) {
            // get challenge
            let getChallengeArgs = {
                data: {
                    name: base.testAccountName
                 },
                 headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.accountTenderPort + '/account/challenge', getChallengeArgs, function(data) {
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'account-read');
               // challenge should be base.testChallenge
                done();
            });
        });  // it get challenge block
        
        // post response to challenge to complete account sign in
        it('selection: sign in by posting challenge response', function(done) {
            let postResponseArgs = {
                data: {
                    account: testAccountRef,
                    response: base.testResponse
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/response', postResponseArgs, function(data) {
                testPermit = data.permit;
                assert.ok(data.result === 'okay');
                assert.ok(data.reason === 'response-valid');
                done();
            });
        });  // it posts challenge response and receives permit block

        // plan: post, fetch, post update, fetch again, delete, fetch failure
        // really should check ops with invalid account/permit
        // or badly mangled or malicious input data
        
        // post selection
        it('selection: post selection', function(done) {
            let postArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    context: testContext,
                    context_type: testContextType,
                    field: testField,
                    field_type: testFieldType,
                    field_name: testFieldName
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/selection', postArgs, function(postData) {
                assert.ok(postData.result === 'done');
                testSelectionRef = postData.ref;
                done();
            });
        });

        // get posted selection
        it('selection: fetch returns posted selection data', function(done) {
            let getArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/selection/' + testSelectionRef, getArgs, function(getData) {
                assert.ok(getData.result === 'okay');
                // really need to check returned data
                done();
            });
        });

        // post selection update
        it('selection: post selection update', function(done) {
            let postArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit,
                    field_name: testFieldName + testFieldName
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/selection/' + testSelectionRef, postArgs, function(postData) {
                assert.ok(postData.result === 'done');
                //testSelectionRef = postData.ref;
                done();
            });
        });

        // get posted selection
        it('selection: fetch returns updated selection data', function(done) {
            let getArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/selection/' + testSelectionRef, getArgs, function(getData) {
                assert.ok(getData.result === 'okay');
                // really need to check returned data
                done();
            });
        });

        // delete selection
        // note there is no confirmation step
        it('selection: delete selection', function(done) {
            let deleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.servicePort + '/selection/delete/' + testSelectionRef, deleteArgs, function(deleteData) {
                assert.ok(deleteData.result === 'done');
                done();
            });
        });
        
        // attempt to fetch deleted selection
        it('selection: fetch fails for deleted record', function(done) {
            let getArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.get('http://localhost:' + config.servicePort + '/selection/' + testSelectionRef, getArgs, function(getData) {
                assert.ok(getData.result === 'failed');
                // really need to check returned data
                done();
            });
        });
        
        // all testing done, now clean up environment by deleting account
        // first request account deletion
        it('selection: request test account deletion', function(done) {
            let postDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/delete', postDeleteArgs, function(data) {
                assert.ok(data.result === 'okay');
                done();
            });
        });  // end request delete block
        // complete deletion with confirmation
        it('selection: confirm test account deletion', function(done) {
        let confirmDeleteArgs = {
                data: {
                    account: testAccountRef,
                    permit: testPermit
                },
                headers: {'Content-Type': 'application/json'}
            };
            base.post('http://localhost:' + config.accountTenderPort + '/account/delete', confirmDeleteArgs, function(data) {
                assert.ok(data.result === 'done');
                done();
            });
        });  // end confirm delete block

    });  // describe valid and invalid inspection tests
}    // end of entire testInspection test routine block
selection.test = testSelection;
module.exports = selection;

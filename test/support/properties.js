// properties.js: Test fetching of service properties.
let assert = require('assert');
let config = require('../../app/config');
let base = require('./base');
var properties = {};
function testProperties() {
    // fetch service header
    describe('properties: fetch service header text', function() {
        it('returns correct header string', function(done) {
            base.get('http://localhost:' + config.servicePort + '/property/header-text', { headers: {'Content-Type': 'application/json'} }, function(data) {
                // Version is taken directly from service package data.
                // Possibly name and description should also be recalled from package data.
                let stringData = data.toString();
                let packageObject = require('../../package');
                assert.ok(stringData === '{\'name\': \'' + packageObject.name + '\', \'version\': \'' + packageObject.version + '\', \'description\': \'' + packageObject.description + '\'}');
                done();
           });
        });
    });
}
properties.test = testProperties;
module.exports = properties;


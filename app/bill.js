// bill.js
// basic create, read, update, delete functions
// and business logic relating to billing records
// intended to provide for robust abstraction
//
// !!! none of this code is currently used

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// item object to be exported by module
var bill = {};

// issueValid checks for problems
function billValid() {
}

// billPost to create or update
// pgClient is database
// itemRecord is basic json for database fields
// callback gets notified of result
function billPost(pgClient, billRecord, callback) {
    // debug dump
    pino.info('post bill: %s', JSON.stringify(billRecord));
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (billRecord.ref === undefined || billRecord.ref === '') {
        // no unit ref means create new
        let issueRef = uuidv4();
        let newBillQuery = 'INSERT INTO issue (ref, submitter_name, language, topic, description, notes, status) VALUES (\''
            + issueRef + '\', \'' 
            + billRecord.submitter_name + '\', \'' 
            + billRecord.language + '\',\''
            + billRecord.topic + '\',\'' 
            + billRecord.description + '\',\''
            + billRecord.notes + '\',\''
            + billRecord.status + '\');';
        pino.info('new bill insertion query: %s', newBillQuery);
        pgClient.query(newBillQuery, function(err) {
            if (err) {
                pino.error('error inserting bill data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new bill inserted');
            callback({result: 'done', reason: 'bill-posted', ref: issueRef});
            return;
        });  // end insert bill data block
    }
    else {
        // nonempty reference present means update that record
        var updateQuery = 'UPDATE bill SET ';
        var separatorString = '';
        if (billRecord.language != undefined && issueRecord.language != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'language = \'' + issueRecord.language + '\'';
        }
        if (billRecord.topic != undefined && issueRecord.topic != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'topic = \'' + issueRecord.topic + '\'';
        }
        if (billRecord.description != undefined && billRecord.description != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'description = \'' + issueRecord.description + '\'';
        }
        if (billRecord.status != undefined && billRecord.status != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'status = \'' + billRecord.status + '\'';
        }
        if (billRecord.notes != undefined && billRecord.notes != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'notes = \'' + billRecord.notes + '\'';
        }
        if (billRecord.submitter_name != undefined && billRecord.submitter_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'submitter_name = \'' + issueRecord.submitter_name + '\'';
        }
        if (billRecord.owner_name != undefined && billRecord.owner_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'owner_name = \'' + billRecord.owner_name + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + billRecord.ref + '\';';
        pino.info('update issue query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating issue: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new issue added');
            callback({result: 'done', reason: 'issue-posted', ref: issueRecord.ref});
            return;
        });
    }
}

function billFetch(pgClient, billReference, callback) {
    pino.info('get bill ref = %s', JSON.stringify(billReference));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // get issue record
    var queryQualifiers = 'submitter_name=\'' + issueReference.account + '\'';
    if (issueReference.language != undefined && issueReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' + issueReference.language + '\'';
    }
    if (issueReference.ref != undefined && issueReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + issueReference.ref + '\'';
    }
    if (issueReference.topic != undefined && issueReference.topic != '') {
        queryQualifiers = queryQualifiers + ' AND topic=\'' + issueReference.topic + '\'';
    }
    if (issueReference.description != undefined && issueReference.description != '') {
        queryQualifiers = queryQualifiers + ' AND description=\'' + issueReference.description + '\'';
    }
    if (issueReference.status != undefined && issueReference.status != '') {
        queryQualifiers = queryQualifiers + ' AND status=\'' + issueReference.status + '\'';
    }
    let issueQuery = 'SELECT ref,topic,description,status FROM issue WHERE '
        + queryQualifiers + ';';
    pino.info('get issue query: %s', issueQuery);
    pgClient.query(issueQuery, function(err, result) {
         if (err) {
            pino.error('error reading issue data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('issue count: %d, data: %s', result.rowCount, JSON.stringify(result.rows));
        callback({result: 'done', reason: 'issue-read',
            count: result.rowCount, data: result.rows
        });
        return;
    });  // end issue data query block
}

function billDelete(pgClient, billReference, callback) {
    pino.info('bill delete, ref=%s', billReference.ref);
    // delete place record
    var queryQualifiers = '';
    if (billReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + billReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM bill '
        + queryQualifiers + ';';
    pino.info('delete bill query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting bill data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        //pino.info('bill data:',result);
        callback({result: 'done', reason: 'bill-deleted'});
        return;
    });  // end deletion query block
}

bill.post = billPost;
bill.get = billFetch;
bill.delete = billDelete;
module.exports = bill;

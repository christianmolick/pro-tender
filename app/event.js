// event.js
// basic create, read, update, delete functions
// and business logic relating to event records
// intended to provide for robust abstraction

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// eventPost to create or update
// pgClient is database
// itemRecord is basic json for database fields
// callback gets notified of result
function eventPost(pgClient, eventRecord, callback) {
    // log invocation
    pino.info('post event: %s', JSON.stringify(eventRecord));
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (eventRecord.ref === undefined || eventRecord.ref === '') {
        // no unit ref means create new
        let eventRef = uuidv4();
        let newEventFields = 'ref,language';
        let newEventValues = '\'' + eventRef + '\', \''
            + eventRecord.language + '\'';
        if (eventRecord.container !== undefined) {
            newEventFields = newEventFields + ',container';
            newEventValues = newEventValues + ', \'' + eventRecord.container + '\'';
        }
        if (eventRecord.summary !== undefined) {
            newEventFields = newEventFields + ',summary';
            newEventValues = newEventValues + ', \'' + eventRecord.summary + '\'';
        }
        if (eventRecord.details !== undefined) {
            newEventFields = newEventFields + ',details';
            newEventValues = newEventValues + ', \'' + eventRecord.details + '\'';
        }
        if (eventRecord.place !== undefined) {
            newEventFields = newEventFields + ',place';
            newEventValues = newEventValues + ', \'' + eventRecord.place + '\'';
        }
        if (eventRecord.person !== undefined) {
            newEventFields = newEventFields + ',person';
            newEventValues = newEventValues + ', \'' + eventRecord.person + '\'';
        }
        if (eventRecord.client !== undefined) {
            newEventFields = newEventFields + ',client';
            newEventValues = newEventValues + ', \'' + eventRecord.client + '\'';
        }
        if (eventRecord.provider !== undefined) {
            newEventFields = newEventFields + ',provider';
            newEventValues = newEventValues + ', \'' + eventRecord.provider + '\'';
        }
        if (eventRecord.process !== undefined) {
            newEventFields = newEventFields + ',process';
            newEventValues = newEventValues + ', \'' + eventRecord.process + '\'';
        }
        if (eventRecord.proposed !== undefined) {
            newEventFields = newEventFields + ',proposed';
            newEventValues = newEventValues + ', \'' + eventRecord.proposed + '\'';
        }
        if (eventRecord.confirmed !== undefined) {
            newEventFields = newEventFields + ',confirmed';
            newEventValues = newEventValues + ', \'' + eventRecord.confirmed + '\'';
        }
        if (eventRecord.expected !== undefined) {
            newEventFields = newEventFields + ',expected';
            newEventValues = newEventValues + ', \'' + eventRecord.expected + '\'';
        }
        if (eventRecord.arrived !== undefined) {
            newEventFields = newEventFields + ',arrived';
            newEventValues = newEventValues + ', \'' + eventRecord.arrived + '\'';
        }
        if (eventRecord.started !== undefined) {
            newEventFields = newEventFields + ',started';
            newEventValues = newEventValues + ', \'' + eventRecord.started + '\'';
        }
        if (eventRecord.completed !== undefined) {
            newEventFields = newEventFields + ',completed';
            newEventValues = newEventValues + ', \'' + eventRecord.completed + '\'';
        }
        if (eventRecord.billed !== undefined) {
            newEventFields = newEventFields + ',billed';
            newEventValues = newEventValues + ', \'' + eventRecord.billed + '\'';
        }
        if (eventRecord.paid !== undefined) {
            newEventFields = newEventFields + ',paid';
            newEventValues = newEventValues + ', \'' + eventRecord.paid + '\'';
        }
        if (eventRecord.reviewed !== undefined) {
            newEventFields = newEventFields + ',summary';
            newEventValues = newEventValues + ', \'' + eventRecord.reviewed + '\'';
        }
        let newEventQuery = 'INSERT INTO event (' + newEventFields
            + ') VALUES (' + newEventValues + ');';
        pino.info('new event insertion query: %s', newEventQuery);
        pgClient.query(newEventQuery, function(err) {
            if (err) {
                pino.error('error inserting event data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new event inserted');
            callback({result: 'done', reason: 'event-posted', ref: eventRef});
            return;
        });  // end insert event data block
    }
    else {
        // nonempty reference present means update that record
        // update query starts with language because it is required?
        var updateQuery = 'UPDATE event SET modified = CURRENT_TIMESTAMP ';
        if (eventRecord.language != undefined && eventRecord.language != '') {
            updateQuery = updateQuery + ' , language = \'' + eventRecord.language + '\'';
        }
        if (eventRecord.container != undefined && eventRecord.container != '') {
            updateQuery = updateQuery + ' , container = \'' + eventRecord.container + '\'';
        }
        if (eventRecord.summary != undefined && eventRecord.summary != '') {
            updateQuery = updateQuery + ' , summary = \'' + eventRecord.summary + '\'';
        }
        if (eventRecord.details != undefined && eventRecord.details != '') {
            updateQuery = updateQuery + ' , details = \'' + eventRecord.details + '\'';
        }
        if (eventRecord.place != undefined && eventRecord.place != '') {
            updateQuery = updateQuery + ' , place = \'' + eventRecord.place + '\'';
        }
        if (eventRecord.person != undefined && eventRecord.person != '') {
            updateQuery = updateQuery + ' , person = \'' + eventRecord.person + '\'';
        }
        if (eventRecord.client != undefined && eventRecord.client != '') {
            updateQuery = updateQuery + ' , client = \'' + eventRecord.client + '\'';
        }
        if (eventRecord.provider != undefined && eventRecord.provider != '') {
            updateQuery = updateQuery + ' , provider = \'' + eventRecord.provider + '\'';
        }
        if (eventRecord.process != undefined && eventRecord.process != '') {
            updateQuery = updateQuery + ' , process = \'' + eventRecord.process + '\'';
        }
        if (eventRecord.proposed != undefined && eventRecord.proposed != '') {
            updateQuery = updateQuery + ' , proposed = \'' + eventRecord.proposed + '\'';
        }
        if (eventRecord.confirmed != undefined && eventRecord.confirmed != '') {
            updateQuery = updateQuery + ' , confirmed = \'' + eventRecord.confirmed + '\'';
        }
        if (eventRecord.expected != undefined && eventRecord.expected != '') {
            updateQuery = updateQuery + ' , expected = \'' + eventRecord.expected + '\'';
        }
        if (eventRecord.arrived != undefined && eventRecord.arrived != '') {
            updateQuery = updateQuery + ' , arrived = \'' + eventRecord.arrived + '\'';
        }
        if (eventRecord.started != undefined && eventRecord.started != '') {
            updateQuery = updateQuery + ' , started = \'' + eventRecord.started + '\'';
        }
        if (eventRecord.completed != undefined && eventRecord.completed != '') {
            updateQuery = updateQuery + ' , completed = \'' + eventRecord.completed + '\'';
        }
        if (eventRecord.billed != undefined && eventRecord.billed != '') {
            updateQuery = updateQuery + ' , billed = \'' + eventRecord.billed + '\'';
        }
        if (eventRecord.paid != undefined && eventRecord.paid != '') {
            updateQuery = updateQuery + ' , paid = \'' + eventRecord.paid + '\'';
        }
        if (eventRecord.reviewed != undefined && eventRecord.reviewed != '') {
            updateQuery = updateQuery + ' , reviewed = \'' + eventRecord.reviewed + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + eventRecord.ref + '\';';
        pino.info('update event query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating event: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('event updated');
            callback({result: 'done', reason: 'event-posted', ref: eventRecord.ref});
            return;
        });
    }
}

function eventFetch(pgClient, eventReference, callback) {
    pino.info('get event ref = %s', JSON.stringify(eventReference));
    // parameters in req.body: session_tag, account_name,
    // and optionally name and/or full_name
    // get event record
    var queryQualifiers = 'language=\'' + eventReference.language + '\'';
    if (eventReference.ref != undefined && eventReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + eventReference.ref + '\'';
    }
    if (eventReference.container != undefined && eventReference.container != '') {
        queryQualifiers = queryQualifiers + ' AND container=\'' + eventReference.container + '\'';
    }
    if (eventReference.summary != undefined && eventReference.summary != '') {
        queryQualifiers = queryQualifiers + ' AND summary=\'' + eventReference.summary + '\'';
    }
    if (eventReference.details != undefined && eventReference.details != '') {
        queryQualifiers = queryQualifiers + ' AND details=\'' + eventReference.details + '\'';
    }
    if (eventReference.place != undefined && eventReference.place != '') {
        queryQualifiers = queryQualifiers + ' AND place=\'' + eventReference.place + '\'';
    }
    if (eventReference.person != undefined && eventReference.person != '') {
        queryQualifiers = queryQualifiers + ' AND person=\'' + eventReference.person + '\'';
    }
    if (eventReference.client != undefined && eventReference.client != '') {
        queryQualifiers = queryQualifiers + ' AND client=\'' + eventReference.client + '\'';
    }
    if (eventReference.provider != undefined && eventReference.provider != '') {
        queryQualifiers = queryQualifiers + ' AND provider=\'' + eventReference.provider + '\'';
    }
    if (eventReference.process != undefined && eventReference.process != '') {
        queryQualifiers = queryQualifiers + ' AND process=\'' + eventReference.process + '\'';
    }

    if (eventReference.proposed != undefined && eventReference.proposed != '') {
        queryQualifiers = queryQualifiers + ' AND proposed=\'' + eventReference.proposed + '\'';
    }
    if (eventReference.confirmed != undefined && eventReference.confirmed != '') {
        queryQualifiers = queryQualifiers + ' AND confirmed=\'' + eventReference.confirmed + '\'';
    }
    if (eventReference.expected != undefined && eventReference.expected != '') {
        queryQualifiers = queryQualifiers + ' AND expected=\'' + eventReference.expected + '\'';
    }
    if (eventReference.arrived != undefined && eventReference.arrived != '') {
        queryQualifiers = queryQualifiers + ' AND arrived=\'' + eventReference.arrived + '\'';
    }
    if (eventReference.started != undefined && eventReference.started != '') {
        queryQualifiers = queryQualifiers + ' AND started=\'' + eventReference.started + '\'';
    }
    if (eventReference.completed != undefined && eventReference.completed != '') {
        queryQualifiers = queryQualifiers + ' AND completed=\'' + eventReference.completed + '\'';
    }
    if (eventReference.billed != undefined && eventReference.billed != '') {
        queryQualifiers = queryQualifiers + ' AND billed=\'' + eventReference.billed + '\'';
    }
    if (eventReference.paid != undefined && eventReference.paid != '') {
        queryQualifiers = queryQualifiers + ' AND paid=\'' + eventReference.paid + '\'';
    }
    if (eventReference.reviewed != undefined && eventReference.reviewed != '') {
        queryQualifiers = queryQualifiers + ' AND reviewed=\'' + eventReference.reviewed + '\'';
    }
    let eventQuery = 'UPDATE event SET viewed = CURRENT_TIMESTAMP WHERE '
        + queryQualifiers + '; '
        + 'SELECT ref,language,container,summary,details,place,person,client,provider,process,proposed,confirmed,expected,arrived,started,completed,billed,paid,reviewed FROM event WHERE '
        + queryQualifiers + ';';
    pino.info('get event query: %s', eventQuery);
    pgClient.query(eventQuery, function(err, result) {
         if (err) {
            pino.error('error reading event data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('event count: %d, data: %s', result[1].rowCount, JSON.stringify(result[1].rows));
        // okay instead of done because no data changed
        callback({result: 'okay', reason: 'event-read',
            count: result[1].rowCount, data: result[1].rows
        });
        return;
    });  // end event data query block
}

function eventDelete(pgClient, eventReference, callback) {
    pino.info('event delete, ref=%s', eventReference.ref);
    // delete place record
    var queryQualifiers = '';
    if (eventReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + eventReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM event '
        + queryQualifiers + ';';
    pino.info('delete event query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting event data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        callback({result: 'done', reason: 'event-deleted'});
        return;
    });  // end event deletion query block
}

// event object for exposing methods
var event = {};
event.post = eventPost;
event.get = eventFetch;
event.delete = eventDelete;
module.exports = event;

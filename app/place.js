// place.js
// basic create, read, update, delete functions
// and business logic relating to place records
// intended to provide for robust abstraction
// assumes permit checks already performed, operates directly
//
// summary: name, full_name, type, description
//
// semantics of place records should be documented
// but here briefly noted:
// multiple records may describe the same
// account name is primary reference for accountability
// places may be heirarchically organized or networked
// records may be private, public, or opened to select viewers

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// placePost creates or updates place records
// pgClient is database interface
// placeRecord is json data
// callback gets result on completion
function placePost(pgClient, placeRecord, callback) {
    // log invocation
    pino.info('place post');
    // sanity/validity check ... enough already?
    // conflict/context check
    // changed to use ref if present or create new if not
    if (placeRecord.ref === undefined || placeRecord.ref === '') {
        // no valid reference means create a whole new record
        // !!! assumes name and language given but this was not yet checked
        let placeRef = uuidv4();
        let newPlaceFields = 'ref,name,language';
        let newPlaceValues = '\'' + placeRef + '\', \''
            + placeRecord.name + '\', \''
            + placeRecord.language + '\'';
        if (placeRecord.full_name !== undefined && placeRecord.full_name !== '') {
            newPlaceFields = newPlaceFields + ',full_name';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.full_name + '\'';
        }
        if (placeRecord.container !== undefined && placeRecord.container !== '') {
            newPlaceFields = newPlaceFields + ',container';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.container + '\'';
        }
        if (placeRecord.account !== undefined && placeRecord.account !== '') {
            newPlaceFields = newPlaceFields + ',account';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.account + '\'';
        }
        if (placeRecord.type !== undefined && placeRecord.type !== '') {
            newPlaceFields = newPlaceFields + ',type';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.type + '\'';
        }
        if (placeRecord.description !== undefined && placeRecord.description !== '') {
            newPlaceFields = newPlaceFields + ',description';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.description + '\'';
        }
        if (placeRecord.notes !== undefined && placeRecord.notes !== '') {
            newPlaceFields = newPlaceFields + ',notes';
            newPlaceValues = newPlaceValues + ', \'' + placeRecord.notes + '\'';
        }
        let newPlaceQuery = 'INSERT INTO place (' + newPlaceFields
            + ') VALUES (' + newPlaceValues + ');';
        pino.info('new place query %s', newPlaceQuery);
        pgClient.query(newPlaceQuery, function(err) {
            if (err) {
                pino.error('error recording new place: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new place recorded');
            callback({result: 'done', reason: 'place-posted', ref: placeRef});
            return;
        });
    }
    else {
        // reference present means update that record
        var updateQuery = 'UPDATE place SET modified = CURRENT_TIMESTAMP ';
        if (placeRecord.name != undefined && placeRecord.name != '') {
            updateQuery = updateQuery + ', name = \'' + placeRecord.name + '\'';
        }
        if (placeRecord.full_name != undefined && placeRecord.full_name != '') {
            updateQuery = updateQuery + ', full_name = \'' + placeRecord.full_name + '\'';
        }
        if (placeRecord.account != undefined && placeRecord.account != '') {
            updateQuery = updateQuery + ', account = \'' + placeRecord.account + '\'';
        }
        if (placeRecord.container != undefined && placeRecord.container != '') {
            updateQuery = updateQuery + ', container = \'' + placeRecord.container + '\'';
        }
        if (placeRecord.language != undefined && placeRecord.language != '') {
            updateQuery = updateQuery + ', language = \'' + placeRecord.language + '\'';
        }
        if (placeRecord.type != undefined && placeRecord.type != '') {
            updateQuery = updateQuery + ', type = \'' + placeRecord.type + '\'';
        }
        if (placeRecord.description != undefined && placeRecord.description != '') {
            updateQuery = updateQuery + ', description = \'' + placeRecord.description + '\'';
        }
        if (placeRecord.notes != undefined && placeRecord.notes != '') {
            updateQuery = updateQuery + ', notes = \'' + placeRecord.notes + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + placeRecord.ref + '\';';
        pino.info('update place query %s', JSON.stringify(updateQuery));
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating new place: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new place added');
            callback({result: 'done', reason: 'place-posted', ref: placeRecord.ref});
            return;
        });
    }
}

// placeFetch gets a record based on a reference
function placeFetch(pgClient, placeReference, callback) {
    // get place record
    // !!! this ref not null is a hack to avoid properly handling the ANDs 
    // !!! more extreme hacking ... hrm
    // !!! switching to account reference start for now, review and refactor!
    var queryQualifiers = 'account=\'' + placeReference.account + '\'';
    if (placeReference.ref != undefined && placeReference.ref) {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + placeReference.ref + '\'';
    }
    if (placeReference.container != undefined && placeReference.container) {
        queryQualifiers = queryQualifiers + ' AND container=\'' + placeReference.container + '\'';
    }
    if (placeReference.name != undefined && placeReference.name) {
        queryQualifiers = queryQualifiers + ' AND name=\'' + placeReference.name + '\'';
    }
    if (placeReference.full_name != undefined && placeReference.full_name) {
        queryQualifiers = queryQualifiers + ' AND full_name=\'' + placeReference.full_name + '\'';
    }
    if (placeReference.language != undefined && placeReference.language) {
        queryQualifiers = queryQualifiers + ' AND language=\'' + placeReference.language + '\'';
    }
    if (placeReference.type != undefined && placeReference.type) {
        queryQualifiers = queryQualifiers + ' AND type=\'' + placeReference.type + '\'';
    }
    if (placeReference.description != undefined && placeReference.description) {
        queryQualifiers = queryQualifiers + ' AND description=\'' + placeReference.description + '\'';
    }
    if (placeReference.notes != undefined && placeReference.notes) {
        queryQualifiers = queryQualifiers + ' AND notes=\'' + placeReference.notes + '\'';
    }
    let placeQuery = 
        'UPDATE place SET viewed = CURRENT_TIMESTAMP WHERE '
        + queryQualifiers + ';'
        + 'SELECT ref,name,full_name,account,container,language,type,description,notes'
        + ' FROM place WHERE ' + queryQualifiers + ';';
    pino.info('get place query: %s', placeQuery);
    pgClient.query(placeQuery, function(err, result) {
        if (err) {
            pino.error('error reading place data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        //pino.info('RAW PLACE FETCH RESULT: %s', JSON.stringify(result));
        pino.info('place data read: count = %d, data = %s', result[1].rowCount, JSON.stringify(result[1].rows));
        // result is okay instead of done because no changes were made
        callback({result: 'okay', reason: 'place-read',
            count: result[1].rowCount, data: result[1].rows
        });
        return;
    });
}

// placeDelete removes a record based on a reference
function placeDelete(pgClient, placeReference, callback) {
    // delete place record
    var queryQualifiers = '';
    if (placeReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + placeReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM place '
        + queryQualifiers + ';';
    pino.info('delete place query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting place data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('deleted place data');
        callback({result: 'done', reason: 'place-deleted'});
        return;
    });
}

// place object to be exported by module
var place = {};
place.post = placePost;
place.get = placeFetch;
place.delete = placeDelete;
module.exports = place;


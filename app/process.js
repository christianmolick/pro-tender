// process.js 
// basic create, read, update, delete functions
// and business logic relating to process records
// intended to provide for robust abstraction

// semantics are important, similar to other records
// may be multiple copies, access restrictions,
// account name reference primary accountability,
// processes may relate to each other in line, graph, or tree
//
// this code is so far unused

// uuidv for references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// processValid is a basic rationality check
function processValid() {
}

// processPost to make or update process records
// pgClient is database, processRecord is json data, callback gets notified
function processPost(pgClient, processRecord, callback) {
    // debug dump
    pino.info('process post');
    // presence check
    let presenceQuery = 'SELECT ref FROM process WHERE brief=\'' + processRecord.brief + '\' OR summary=\'' + processRecord.summary + '\';';
    pino.info('process post presence query:', presenceQuery);
    pgClient.query(presenceQuery, function(err, result) {
        if (err) {
            pino.error('error checking for presence: %s', err);
            callback({result: 'failed', reason: 'process-invalid'});
            return;
        }
        if (result.rowCount === 0) {
            // so such, make new record
            let processRef = uuidv4();
            let newProcessQuery = 'INSERT INTO process (ref, brief, summary) VALUES (\'' 
                + processRef + '\', \'' 
                + processRecord.brief + '\',\'' 
                + processRecord.summary + '\');';
            pino.info('new process insertion query', newProcessQuery);
            pgClient.query(newProcessQuery, function(err) {
                if (err) {
                    pino.error('error inserting process data: %s', err);
                    callback({result: 'failed', reason: 'error'});
                    return;
                }
                pino.info('new process inserted');
                callback({result: 'done', reason: 'process-posted', ref: processRef});
                return;
            });  // end new process record insertion block
        }
        else if (result.rowCount === 1) {
            // unique match found
        }
        else {
            // multiple matches 
        }
    });  // end presence check block
}

// processFetch
function processFetch(pgClient, processReference, callback) {
    pino.info('process fetch');
    pino.info('ref:', processReference.ref);
    // get process record
    var queryQualifiers = '';
    if (processReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + processReference.ref + '\'';
    }
    let processQuery = 'SELECT brief,summary,created FROM process ' 
        + queryQualifiers + ';';
    pino.info('get process query:', processQuery);
    pgClient.query(processQuery, function(err,result) {
        if (err) {
            pino.error('error reading process data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('process data:', result.rows[0]);
        callback({result: 'done', reason: 'process-read'});
        return;
    });  // end get process record query block
}

// processDelete
function processDelete(pgClient, processReference, callback) {
    pino.info('process delete, ref = %s', processReference.ref);
    // delete process record
    var queryQualifiers = '';
    if (processReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + processReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM process ' + queryQualifiers + ';';
    pino.info('delete process query:', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error reading process data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('process deleted');
        callback({result: 'done', reason: 'process-deleted'});
        return;
    });  // end delete process query block
}

// process object to be exported by module
var process = {};
process.post = processPost;
process.get = processFetch;
process.delete = processDelete;
module.exports = process;


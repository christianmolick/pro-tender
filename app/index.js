'use strict';

// internal packages for configuration and basic abstractions
let config = require('./config');
let property = require('./property');
let selection = require('./selection');
let permit = require('./permit');
let person = require('./person');
let place = require('./place');
let item = require('./item');
let issue = require('./issue');
let request = require('./request');
let event = require('./event');
let process = require('./process');

// external packages for logging, uuids, express, postgres
let pino = require('pino')();
// deprecated usage let uuidv4 = require('uuid/v4');
let { v4: uuidv4 } = require('uuid');
let express = require('express');
let app = express();
let http = require('http').Server(app);
let bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
let pg = require('pg');
let pgClient = new pg.Client(config.dbAccessString);
pgClient.connect(function (err) {
    if (err) {
        return pino.error('could not connect to postgres: %s', err);
    }
});
let clientSource = require('node-rest-client').Client;

function getProperty(url, args, callback) {
    var client = new clientSource();
    var responseData = {};
    client.get(url, args, function(data, response) {
        if (response.statusCode === 404) {
            callback();
        } else {
            responseData = data;
            callback(responseData);
        }
    });
}

// fetch production service properties 
app.get('/property/:name', function getProductionProperty (req, res) {
    pino.info('GET production property, name = %s', req.params.name);
    property.get(pgClient, req.params.name, function (data) {
        pino.info('fetched property value: %s', JSON.stringify(data));
        res.send(data);
    });
});

// selection process related interactions: post, get, delete
// post selection to create selection record
app.post('/selection', function postSelection (req, res) {
    pino.info('POST /selection, body = %s', JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed: no permit, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result: %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        selection.post(pgClient, req.body, function(result) {
            pino.info('selection post result: %s', JSON.stringify(result));
            res.json(result);
        });
    });  // end permit check block
});

// post updated data to specific selection
app.post('/selection/:ref', function postSelectionUpdate (req, res) {
    pino.info('POST /selection/:ref, ref = %s, body = %s', 
        req.params.ref, JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed: no permit, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // !!! maybe pass reference and updated fields separately
        let selectionRecord = req.body;
        selectionRecord.ref = req.params.ref;
        selection.post(pgClient, selectionRecord, function(result) {
            pino.info('selection post update result: %s', JSON.stringify(result));
            res.json(result);
        });
    });  // end permit check block
});

// get selection data
app.get('/selection/:ref', function getSelection (req, res) {
    pino.info('GET /selection/:ref, ref = %s', 
        req.params.ref, JSON.stringify(req.body));
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed: no permit, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }    
        selection.get(pgClient, req.params.ref,function(result) {
            pino.info('selection get result: %s', JSON.stringify(result));
            res.json(result);
        });
    });  // end permit check block
});

// post deletion of specific selection
app.post('/selection/delete/:ref', function postSelectionUpdate (req, res) {
    pino.info('POST /selection/delete/:ref, ref = %s', 
        req.params.ref);
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed: no permit, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }    
        selection.delete(pgClient, req.params.ref, function(result) {
            pino.info('selection post result: %s', JSON.stringify(result));
            res.json(result);
        });
    });  // end permit check block
});

// get/post/delete /person
app.post('/person', function postPerson(req, res) {
    pino.info('POST /person with message = %s', JSON.stringify(req.body));
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed: no permit, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let personRecord = {};
        if (req.body.ref !== undefined && req.body.ref !== '' ) {
            personRecord.ref = req.body.ref;
        }
        if (req.body.language !== undefined && req.body.language !== '' ) {
            personRecord.language = req.body.language;
        }
        if (req.body.name !== undefined && req.body.name !== '' ) {
            personRecord.name = req.body.name;
        }
        if (req.body.full_name !== undefined && req.body.full_name !== '' ) {
            personRecord.full_name = req.body.full_name;
        }
        if (req.body.account !== undefined && req.body.account !== '' ) {
            personRecord.account = req.body.account;
        }
        if (req.body.container !== undefined && req.body.container !== '' ) {
            personRecord.container = req.body.container;
        }
        if (req.body.place !== undefined && req.body.place !== '' ) {
            personRecord.place = req.body.place;
        }
        if (req.body.profile !== undefined && req.body.profile !== '' ) {
            personRecord.profile = req.body.profile;
        }
        if (req.body.preferences !== undefined && req.body.preferences !== '' ) {
            personRecord.preferences = req.body.preferences;
        }
        if (req.body.provides !== undefined && req.body.provides !== '' ) {
            personRecord.provides = req.body.provides;
        }
        pino.info('posting person record: %s', JSON.stringify(personRecord));
        person.post(pgClient, personRecord, function (postResult) {
            pino.info('person post returned: %s', JSON.stringify(postResult));
            // check on result before returning !!!
            res.json(postResult);
            //res.json({result: 'done', reason: 'person-posted'});
            return;
        });
    });  // end permit check block
});

app.get('/person', function getPerson(req, res) {
    // parameters in req.body: ref, session_tag, account,
    // and optionally name and/or full_name
    // result to be potentially large number of records
    pino.info('GET /person, body = %s', JSON.stringify(req.body));
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error permit required but not present');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get person record
        var personRecord = {};
        if (req.body.ref != undefined || req.body.ref != '') {
            personRecord['ref'] = req.body.ref;
        }
        if (req.body.name != undefined || req.body.name != '') {
            personRecord['name'] = req.body.name;
        }
        if (req.body.full_name != undefined || req.body.full_name != '') {
            personRecord['full_name'] = req.body.full_name;
        }
        if (req.body.account != undefined || req.body.account != '') {
            personRecord['account'] = req.body.account;
        }
        if (req.body.language != undefined || req.body.language != '') {
            personRecord['language'] = req.body.language;
        }
        if (req.body.container != undefined || req.body.container != '') {
            personRecord['container'] = req.body.container;
        }
        if (req.body.place != undefined || req.body.place != '') {
            personRecord['place'] = req.body.place;
        }
        if (req.body.profile != undefined || req.body.profile != '') {
            personRecord['profile'] = req.body.profile;
        }
        if (req.body.preferences != undefined || req.body.preferences != '') {
            personRecord['preferences'] = req.body.preferences;
        }
        if (req.body.provides != undefined || req.body.provides != '') {
            personRecord['provides'] = req.body.provides;
        }
        person.get(pgClient, personRecord, function (getResult) {
            pino.info('person get returned %s', JSON.stringify(getResult));
            // check on result before returning !!!
            res.json(getResult);
            return;
        });
    });  // end permit check block
});

app.get('/person/summary', function getPersonSummary(req, res) {
    pino.info('GET /person/summary, body = %s', JSON.stringify(req.body));
    // sanity check parameters
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error permit required but not present');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get person record
        var personRecord
        if (req.body.ref != undefined || req.body.ref != '') {
            personRecord['ref'] = req.body.ref;
        }
        // !!! else no person reference, so fail gracefully
        person.get(pgClient, personRecord, function (fetchResult) {
            pino.info('person get for summary returned %s', JSON.stringify(fetchResult));
            // check on result before returning !!!
            // ref, container, name, full_name, language, account, place,
            // created, viewed, modified, profile, preferences, provides
            res.json(fetchResult.name + ', ' + fetchResult.full_name + ', ' + fetchResult.profile);
            return;
        });
    });  // end permit check block
});

// delete with post /person/delete because result may bounce
app.post('/person/delete', function deletePerson(req, res) {
    pino.info('DELETE /person');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // !!! ref also absolutely required for this to work
   // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        var personRecord = {
            ref: req.body.ref
        };
        person.delete(pgClient, personRecord, function(deleteResult) {
            pino.info('person delete returned: %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
}); 

// get/post/delete /place
app.post('/place', function postPlace(req, res) {
    // log invocation
    pino.info('POST /place');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(checkResult) {
        if (checkResult.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let placeRecord = {};
        if (req.body.ref !== undefined && req.body.ref !== '') {
            placeRecord.ref = req.body.ref;
        }
        if (req.body.container !== undefined && req.body.container !== '') {
            placeRecord.container = req.body.container;
        }
        if (req.body.language !== undefined && req.body.language !== '') {
            placeRecord.language = req.body.language;
        }
        if (req.body.name !== undefined && req.body.name !== '') {
            placeRecord.name = req.body.name;
        }
        if (req.body.full_name !== undefined && req.body.full_name !== '') {
            placeRecord.full_name = req.body.full_name;
        }
        if (req.body.account !== undefined && req.body.account !== '') {
            placeRecord.account = req.body.account;
        }
        if (req.body.type !== undefined && req.body.type !== '') {
            placeRecord.type = req.body.type;
        }
        if (req.body.description !== undefined && req.body.description !== '') {
            placeRecord.description = req.body.description;
        }
        if (req.body.notes !== undefined && req.body.notes !== '') {
            placeRecord.notes = req.body.notes;
        }        
        place.post(pgClient, placeRecord, function(postResult) {
            pino.info('place post result %s', JSON.stringify(postResult));
            res.json(postResult);
            return;
        });
    });  // end permit check block
})
    
app.get('/place', function getPlace(req, res) {
    pino.info('GET /place: %s', JSON.stringify(req.body));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('get place failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('get place failed, account required');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        var placeRecord = {};
        // strictly speaking this logic could leave an empty record
        if (req.body.ref != undefined || req.body.ref != '') {
            placeRecord['ref'] = req.body.ref;
        }
        if (req.body.container != undefined || req.body.container != '') {
            placeRecord['container'] = req.body.container;
        }
        if (req.body.account != undefined || req.body.account != '') {
            placeRecord['account'] = req.body.account;
        }
        if (req.body.name != undefined || req.body.name != '') {
            placeRecord['name'] = req.body.name;
        }
        if (req.body.full_name != undefined || req.body.full_name != '') {
            placeRecord['full_name'] = req.body.full_name;
        }
        if (req.body.language != undefined || req.body.language != '') {
            placeRecord['language'] = req.body.language;
        }
        if (req.body.type != undefined || req.body.type != '') {
            placeRecord['type'] = req.body.type;
        }
        if (req.body.description != undefined || req.body.description != '') {
            placeRecord['description'] = req.body.description;
        }
        if (req.body.notes != undefined || req.body.notes != '') {
            placeRecord['notes'] = req.body.notes;
        }
        place.get(pgClient, placeRecord, function(getResult) {
            pino.info('place fetch returned %s', JSON.stringify(getResult));
            res.json(getResult);
            return;
        });
    });  // end permit check block
});

app.get('/place/summary', function getPlaceSummary(req, res) {
    pino.info('GET /place/summary, body = %s', JSON.stringify(req.body));
    // sanity check parameters
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error permit required but not present');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get place record
        var placeRecord
        if (req.body.ref != undefined || req.body.ref != '') {
            placeRecord['ref'] = req.body.ref;
        }
        // !!! else no place reference, so fail gracefully
        place.get(pgClient, placeRecord, function (fetchResult) {
            pino.info('place get for summary returned %s', JSON.stringify(fetchResult));
            // check on result before returning !!!
            // ref, container, name, full_name, language, account,
            // created, viewed, modified, type, description, notes
            res.json(fetchResult.name + ', ' + fetchResult.full_name + ', ' + fetchResult.type + ', ' + fetchResult.description);
            return;
        });
    });  // end permit check block
});

app.post('/place/delete', function deletePlace(req, res) {
    pino.info('DELETE /place: body = %s', JSON.stringify(req.body));
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        var placeRecord = {
            ref: req.body.ref
        };
        place.delete(pgClient, placeRecord, function(deleteResult) {
            pino.info('delete place result %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});


// BEGIN EVENT BLOCK
// get/post/delete /event
app.post('/event', function postEvent(req, res) {
    pino.info('POST /event');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('event post failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // also check that account is nonempty?
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let eventRecord = {};
        if (req.body.ref !== undefined && req.body.ref !== '') {
            eventRecord.ref = req.body.ref;
        }
        if (req.body.container !== undefined && req.body.container !== '') {
            eventRecord.container = req.body.container;
        }
        if (req.body.language !== undefined && req.body.language !== '') {
            eventRecord.language = req.body.language;
        }
        if (req.body.summary !== undefined && req.body.summary !== '') {
            eventRecord.summary = req.body.summary;
        }
        if (req.body.details !== undefined && req.body.details !== '') {
            eventRecord.details = req.body.details;
        }
        if (req.body.place !== undefined && req.body.place !== '') {
            eventRecord.place = req.body.place;
        }
        if (req.body.person !== undefined && req.body.person !== '') {
            eventRecord.person = req.body.person;
        }
        if (req.body.client !== undefined && req.body.client !== '') {
            eventRecord.client = req.body.client;
        }
        if (req.body.provider !== undefined && req.body.provider !== '') {
            eventRecord.provider = req.body.provider;
        }
        if (req.body.proposed !== undefined && req.body.proposed !== '') {
            eventRecord.proposed = req.body.proposed;
        }
        if (req.body.confirmed !== undefined && req.body.confirmed !== '') {
            eventRecord.confirmed = req.body.confirmed;
        }
        if (req.body.expected !== undefined && req.body.expected !== '') {
            eventRecord.expected = req.body.expected;
        }
        if (req.body.arrived !== undefined && req.body.arrived !== '') {
            eventRecord.arrived = req.body.arrived;
        }
        if (req.body.started !== undefined && req.body.started !== '') {
            eventRecord.started = req.body.started;
        }
        if (req.body.completed !== undefined && req.body.completed !== '') {
            eventRecord.completed = req.body.completed;
        }
        if (req.body.billed !== undefined && req.body.billed !== '') {
            eventRecord.billed = req.body.billed;
        }
        if (req.body.paid !== undefined && req.body.paid !== '') {
            eventRecord.paid = req.body.paid;
        }
        if (req.body.reviewed !== undefined && req.body.reviewed !== '') {
            eventRecord.reviewed = req.body.reviewed;
        }
        event.post(pgClient, eventRecord, function (postResult) {
            pino.info('event post returned %s', JSON.stringify(postResult));
            res.json(postResult);
            return;
        });
    });  // end permit check block
});  // end post event block

app.get('/event', function getEvent(req, res) {
    pino.info('GET /event, parms = %s', JSON.stringify(req.body));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result = %s', JSON.stringify(check.result));
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get event record
        var eventRecord = {};
        if (req.body.ref !== undefined && req.body.ref !== '') {
            eventRecord.ref = req.body.ref;
        }
        if (req.body.container !== undefined && req.body.container !== '') {
            eventRecord.container = req.body.container;
        }
        if (req.body.language !== undefined && req.body.language !== '') {
            eventRecord.language = req.body.language;
        }
        if (req.body.summary !== undefined && req.body.summary !== '') {
            eventRecord.summary = req.body.summary;
        }
        if (req.body.details !== undefined && req.body.details !== '') {
            eventRecord.details = req.body.details;
        }
        if (req.body.place !== undefined && req.body.place !== '') {
            eventRecord.place = req.body.place;
        }
        if (req.body.person !== undefined && req.body.person !== '') {
            eventRecord.person = req.body.person;
        }
        if (req.body.client !== undefined && req.body.client !== '') {
            eventRecord.client = req.body.client;
        }
        if (req.body.provider !== undefined && req.body.provider !== '') {
            eventRecord.provider = req.body.provider;
        }
        if (req.body.process !== undefined && req.body.process !== '') {
            eventRecord.process = req.body.process;
        }
        if (req.body.proposed !== undefined && req.body.proposed !== '') {
            eventRecord.proposed = req.body.proposed;
        }
        if (req.body.confirmed !== undefined && req.body.confirmed !== '') {
            eventRecord.confirmed = req.body.confirmed;
        }
        if (req.body.expected !== undefined && req.body.expected !== '') {
            eventRecord.expected = req.body.expected;
        }
        if (req.body.arrived !== undefined && req.body.arrived !== '') {
            eventRecord.arrived = req.body.arrived;
        }
        if (req.body.started !== undefined && req.body.started !== '') {
            eventRecord.started = req.body.started;
        }
        if (req.body.completed !== undefined && req.body.completed !== '') {
            eventRecord.completed = req.body.completed;
        }
        if (req.body.billed !== undefined && req.body.billed !== '') {
            eventRecord.billed = req.body.billed;
        }
        if (req.body.paid !== undefined && req.body.paid !== '') {
            eventRecord.paid = req.body.paid;
        }
        if (req.body.reviewed !== undefined && req.body.reviewed !== '') {
            eventRecord.reviewed = req.body.reviewed;
        }
        pino.info('fetching event with record = %s', JSON.stringify(eventRecord));
        event.get(pgClient, eventRecord, function(getResult) {
            pino.info('event fetch returned %s', JSON.stringify(getResult));
            res.json({result: 'okay', reason: 'event-read',
                count: getResult.count, data: getResult.data
            });
            return;
        });    
    });  // end permit check block
});  // end get event data block


app.get('/event/summary', function getEventSummary(req, res) {
    pino.info('GET /event/summary, body = %s', JSON.stringify(req.body));
    // sanity check parameters
    if (req.body.account === undefined || req.body.account === '') {
        pino.error('error account required but not present');
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('error permit required but not present');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function (check) {
        pino.info('permit check result %s', JSON.stringify(check));
        if (check.result != 'okay') {
            pino.error('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get event record
        var eventRecord
        if (req.body.ref != undefined || req.body.ref != '') {
            eventRecord['ref'] = req.body.ref;
        }
        // !!! else no event reference, so fail gracefully
        event.get(pgClient, eventRecord, function (fetchResult) {
            pino.info('event get for summary returned %s', JSON.stringify(fetchResult));
            // check on result before returning !!!
            // ref, summary, place, person, client, provider,
            // proposed, confirmed, expected, completed
            // need summaries for place, person, client, provider
            res.json(fetchResult.summary + ', ' + fetchResult.full_name + ', ' + fetchResult.type + ', ' + fetchResult.description);
            return;
        });
    });  // end permit check block
});

app.post('/event/delete', function deleteEvent(req, res) {
    pino.info('DELETE /event, ref=%s', req.body.ref);
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info('delete request permit check result = %s', check.result);
        if (check.result != 'okay') {
            return;
        }
        var eventRecord = {
            ref: req.body.ref
        };
        event.delete(pgClient, eventRecord, function(deleteResult) {
            pino.info('event delete result %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});  // end delete event block
// END OF EVENT BLOCK

// !!! below this point not currently used, tested, or refactored for account ref
//     item, issue, request, process, review, bill all just sketched in for now

// get/post/delete /item
app.post('/item', function postItem(req, res) {
    pino.info('POST /item');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('item post failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', checkResult);
        if (checkResult.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let itemRecord = {
            ref: req.body.ref,
            name: req.body.name,
            full_name: req.body.full_name,
            language: req.body.language,
            account: req.body.account
            
        };
        item.post(pgClient, itemRecord, function (postResult) {
            pino.info('item post returned', postResult);
            res.json(postResult);
            return;
        });
    });  // end permit check block
});  // end post item block

app.get('/item', function getItem(req, res) {
    pino.info('GET /item');
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result = %s', JSON.stringify(check.result));
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get item record
        var itemRecord = {
            account: req.body.account,
            ref: req.body.ref,
            name: req.body.name,
            full_name: req.body.full_name,
            language: req.body.language,
            type: req.body.type,
            description: req.body.description
        };
        item.get(pgClient, itemRecord, function(getResult) {
            pino.info('item fetch returned %s', JSON.stringify(getResult));
            res.json({result: 'okay', reason: 'item-read',
                count: getResult.count, data: getResult.data
            });
            return;
        });    
    });  // end permit check block
});  // end get item data block

app.post('/item/delete', function deleteItem(req, res) {
    pino.info('DELETE /item');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-name-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info();
        if (check.result != 'okay') {
        }
        var itemRecord = {
            ref: req.body.ref
        };
        item.delete(pgClient, itemRecord, function(deleteResult) {
            pino.info('item delete result %s', deleteResult);
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});  // end delete item block

// ISSUE STUFF
// get/post/delete /item
app.post('/issue', function postItem(req, res) {
    pino.info('POST /issue');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('issue post failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let issueRecord = {
            ref: req.body.ref,
            language: req.body.language,
            submitter_name: req.body.account,
            topic: req.body.topic,
            description: req.body.description
        };
        issue.post(pgClient, issueRecord, function (postResult) {
            pino.info('issue post returned %s', JSON.stringify(postResult));
            res.json(postResult);
            return;
        });
    });  // end permit check block
});  // end post item block

app.get('/issue', function getIssue(req, res) {
    pino.info('GET /issue, parms = %s', JSON.stringify(req.body));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result = %s', JSON.stringify(check.result));
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get issue record
        var issueRecord = {
            account: req.body.account,
            ref: req.body.ref,
            name: req.body.name,
            full_name: req.body.full_name,
            language: req.body.language,
            type: req.body.type,
            description: req.body.description
        };
        pino.info('fetching issue with record = %s', JSON.stringify(issueRecord));
        issue.get(pgClient, issueRecord, function(getResult) {
            pino.info('issue fetch returned %s', JSON.stringify(getResult));
            res.json({result: 'okay', reason: 'issue-read',
                count: getResult.count, data: getResult.data
            });
            return;
        });    
    });  // end permit check block
});  // end get item data block

app.post('/issue/delete', function deleteIssue(req, res) {
    pino.info('DELETE /issue, ref=%s', req.body.ref);
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-name-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info('delete issue permit check result = %s', check.result);
        if (check.result != 'okay') {
            return;
        }
        var issueRecord = {
            ref: req.body.ref
        };
        issue.delete(pgClient, issueRecord, function(deleteResult) {
            pino.info('issue delete result %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});  // end delete item block

// REQUEST SPECIFIC
// get/post/delete /request
app.post('/request', function postItem(req, res) {
    pino.info('POST /request');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.error('request post failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function (checkResult) {
        pino.info('permit check result %s', JSON.stringify(checkResult));
        if (checkResult.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let requestRecord = {
            ref: req.body.ref,
            language: req.body.language,
            submitter: req.body.account,
            topic: req.body.topic,
            description: req.body.description
        };
        request.post(pgClient, requestRecord, function (postResult) {
            pino.info('request post returned', postResult);
            res.json(postResult);
            return;
        });
    });  // end permit check block
});  // end post item block

app.get('/request', function getRequest(req, res) {
    pino.info('GET /request, parms = %s', JSON.stringify(req.body));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-name-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result = %s', JSON.stringify(check.result));
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get request record
        var requestRecord = {
            account: req.body.account,
            ref: req.body.ref,
            name: req.body.name,
            full_name: req.body.full_name,
            language: req.body.language,
            type: req.body.type,
            description: req.body.description
        };
        pino.info('fetching request with record = %s', JSON.stringify(requestRecord));
        request.get(pgClient, requestRecord, function(getResult) {
            pino.info('request fetch returned %s', JSON.stringify(getResult));
            res.json({result: 'okay', reason: 'request-read',
                count: getResult.count, data: getResult.data
            });
            return;
        });    
    });  // end permit check block
});  // end get request data block

app.post('/request/delete', function deleteRequest(req, res) {
    pino.info('DELETE /request, ref=%s', req.body.ref);
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined && req.body.account === '') {
        res.json({result: 'failed', reason: 'account-name-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info('delete request permit check result = %s', check.result);
        if (check.result != 'okay') {
            return;
        }
        var requestRecord = {
            ref: req.body.ref
        };
        request.delete(pgClient, requestRecord, function(deleteResult) {
            pino.info('request delete result %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});  // end delete request block

// get/post/delete /process
app.post('/process', function postProcess(req, res) {
    pino.info('POST /process');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        pino.info('failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // account name required?
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info('permit check returned', check.result);
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        let processRecord = {
            brief:      req.body.brief,
            summary:    req.body.summary,
            client:     req.body.client,
            provider:   req.body.provider,
            place:      req.body.place,
            target:     req.body.target,
            start:      req.body.start,
            duration:   req.body.duration,
            fee:        req.body.fee
        };
        process.post(pgClient, processRecord, function(postResult) {
            pino.info('process post returned', postResult);
            res.json(postResult);
            return;
        });
    });  // end permit check block
});

app.get('/process', function getProcess(req, res) {
    pino.info('GET /process');
    pino.info('ref:', req.body.ref);
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit === '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        res.json({result: 'failed', reason: 'reference-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'read', function(check) {
        pino.info('permit check result %s', check.result);
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get process record
        let processRecord = {
            ref: req.body.ref
        };
        process.get(pgClient, processRecord, function(getResult) {
            pino.info('process get returned', getResult);
            res.json(getResult);
            return;
        });
    });  // end check permit block
});  // end get process block

app.post('/process/delete', function deleteProcess(req, res) {
    pino.info('DELETE /process, ref =', req.body.ref);
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        pino.info('failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    if (req.body.account === undefined || req.body.account === '') {
        res.json({result: 'failed', reason: 'account-name-required'});
        return;
    }
    // check permit
    permit.check(req.body.account, req.body.permit, 'write', function(check) {
        pino.info('');
        if (check.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // delete process record
        let processRecord = {
            ref: req.body.ref
        };
        process.delete(pgClient, processRecord, function(deleteResult) {
            pino.info('process delete result %s', JSON.stringify(deleteResult));
            res.json(deleteResult);
            return;
        });
    });  // end permit check block
});

// get/post/delete /review
app.post('/review', function postReview(req, res) {
    pino.info('POST /review');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit -- change to use permit.check
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    // !!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // check for duplicate/overlapping reviews, conflicts
        const duplicateQuery = 'SELECT ref FROM review WHERE name=\'' 
            + req.body.name + '\' OR full_name=\'' + req.body.full_name + '\';';
        pino.info('review post duplicate query:', duplicateQuery);
        pgClient.query(duplicateQuery, function(err, result) {
            if (err) {
                pino.error('error checking for duplicate reviews: %s', err);
                res.send({result: 'failed', reason: 'review-invalid'});
                return;
            }
            if (result.rowCount > 0) {
                // !!! conditionally update record
                pino.info('duplicate entry found:',result.rows[0]);
                res.send({result: 'failed', reason: 'review-exists'});
                return;
            }
            // create new review record
            const reviewUuid = uuidv4();
            const newReviewQuery = 'INSERT INTO review (ref, account, name, full_name, type, description) VALUES (\'' 
                + reviewUuid + '\', \'' 
                + req.body.name + '\',\'' 
                + req.body.full_name + '\',\'' 
                + req.body.type + '\',\'' 
                + req.body.description + '\');';
            pino.info('new review insertion query', newReviewQuery);
            // note return value and result both dropped
            pgClient.query(newReviewQuery, function(err) {
                if (err) {
                    pino.error('error inserting review data: %s', err);
                    res.send({result: 'failed', reason: 'error'});
                    return;
                }
                pino.info('new review inserted');
                res.send({result: 'done', reason: 'review-write', ref: reviewUuid});
                return;
            });
        });  // end conflict query block
    });  // end permit check block
});  // post review block

app.get('/review', function getReview(req, res) {
    pino.info('GET /review');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        res.json({result: 'failed', reason: 'session-tag-required'});
        return;
    }
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    // !!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get review record
        var queryQualifiers = '';
        if (req.body.ref) {
            queryQualifiers = 'WHERE ref=\'' + req.body.ref;
        }
        const reviewQuery = 'SELECT name,full_name,type,description,created FROM review ' 
            + queryQualifiers + ';';
        pino.info('get review query:',reviewQuery);
        pgClient.query(reviewQuery, function(err,result) {
            if (err) {
                pino.error('error reading review data: %s', err);
                res.send({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('review data:',result);
            res.send({result: 'done', reason: 'review-read'});
            return;
        });
    });
});

app.delete('/review', function deleteReview(req, res) {
    pino.info('DELETE /review');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        res.json({result: 'failed', reason: 'session-tag-required'});
        return;
    }
    if (req.body.ref === undefined && req.body.account_ref === undefined) {
        res.json({result: 'failed', reason: 'reference-required'});
        return;
    }
    // check permit -- change to use permit.check
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    //!!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            // and reason is permit-valid?
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // delete review record
        var queryQualifiers = '';
        if (req.body.ref) {
            queryQualifiers = 'WHERE ref=\'' + req.body.ref;
        }
        const deletionQuery = 'DELETE FROM review ' + queryQualifiers + ';';
        pino.info('delete review query:',deletionQuery);
        pgClient.query(deletionQuery, function(err) {
            if (err) {
                pino.error('error reading review data: %s', err);
                res.send({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('review data:', result.rows[0]);
            res.send({result: 'done', reason: 'review-deleted'});
            return;
        });
    });
});

// get/post/delete /bill
app.post('/bill', function postBill(req, res) {
    pino.info('POST /bill');
    // sanity check input parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        pino.error('failed, permit required');
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    // check permit -- change to use permit.check
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    // !!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            // and reason is permit-valid?
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // check for duplicate/overlapping bill, conflicts
        // fields: ref, service_ref, created, modified, 
        // amount_payable, amount_received
        // !!! THIS QUERY IS WRONG--RETHINK AND REDO
        const duplicateQuery = 'SELECT ref FROM bill WHERE service_ref=\'' 
        + req.body.event_ref + '\';';
        pino.info('bill post duplicate query: %s', duplicateQuery);
        pgClient.query(duplicateQuery, function(err, result) {
            if (err) {
                pino.error('error checking for duplicate bills: %s', err);
                res.send({result: 'failed', reason: 'bill-invalid'});
                return;
            }
            if (result.rowCount > 0) {
                // !!! conditionally update record
                pino.info('duplicate entry found: %s', JSON.stringify(result.rows[0]));
                res.send({result: 'failed', reason: 'bill-exists'});
                return;
            }
            // create new bill record
            const billUuid = uuidv4();
            // fields: ref, service_ref (event), amount_payable, amount_received
            const newBillQuery = 'INSERT INTO bill (ref, service_ref, amount_payable, amount_received) VALUES (\'' 
                + billUuid + '\', \'' 
                + req.body.event_ref + '\',\'' 
                + req.body.amount_payable + '\',\'' 
                + 0 + '\');';
            pino.info('new bill insertion query', newBillQuery);
            // note return value and result both dropped
            pgClient.query(newBillQuery, function(err) {
                if (err) {
                    pino.error('error inserting bill data: %s', err);
                    res.send({result: 'failed', reason: 'error'});
                    return;
                }
                pino.info('new bill inserted');
                res.send({result: 'done', reason: 'bill-write', ref: billUuid});
                return;
            });
        });
    });
});

app.get('/bill', function getBill(req, res) {
    pino.info('GET /bill');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        res.json({result: 'failed', reason: 'permit-required'});
        return;
    }
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    //!!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // get bill record
        var queryQualifiers = '';
        if (req.body.ref) {
            queryQualifiers = 'WHERE ref=\'' + req.body.ref;
        }
        const billQuery = 'SELECT ref,service_ref,created,modified,amount_payable,amount_received FROM bill ' 
            + queryQualifiers + '\';';
        pino.info('get bill query:',billQuery);
        pgClient.query(billQuery, function(err,result) {
            if (err) {
                pino.error('error reading bill data: %s', err);
                res.send({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('bill data:',result.rows[0]);
            res.send({result: 'okay', reason: 'bill-read'});
            return;
        });
    });
});

app.delete('/bill', function deleteBill(req, res) {
    pino.info('DELETE /ebill');
    // sanity check parameters
    if (req.body.permit === undefined || req.body.permit == '') {
        res.json({result: 'failed', reason: 'session-tag-required'});
        return;
    }
    if (req.body.ref === undefined && req.body.account_ref === undefined) {
        res.json({result: 'failed', reason: 'reference-required'});
        return;
    }
    const checkPermitArgs = {
        data: {
            permit: req.body.permit,
            account: req.body.account
        },
         headers: {'Content-Type': 'application/json'}
    };
    // !!! old broken code not using the modern permit.check method
    getProperty('http://localhost:' + config.accountTenderPort + '/permit/valid', checkPermitArgs, function (data) {
       if (data.result != 'okay') {
            pino.info('failed, permit invalid');
            res.send({result: 'failed', reason: 'permit-invalid'});
            return;
        }
        // delete bill record
        var queryQualifiers = '';
        if (req.body.ref) {
            queryQualifiers = 'WHERE ref=\'' + req.body.ref;
        }
        // other qualifiers
        const deletionQuery = 'DELETE FROM bill ' + queryQualifiers + '\';';
        pino.info('delete bill query:', deletionQuery);
        pgClient.query(deletionQuery, function(err) {
            if (err) {
                pino.error('error reading bill data: %s', err);
                res.send({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('bill deleted');
            res.send({result: 'done', reason: 'bill-deleted'});
            return;
        });
    });
});

// start receiving messages to route
http.listen(config.servicePort, function() {
    pino.info('listening to port %d', config.servicePort);
});

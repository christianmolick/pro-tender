// selection.js
// track information about selection processes in progress

// enable creation of new selection record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// selectionPost enables creating and updating of selection records
function selectionPost(pgClient, selectionRecord, callback) {
    if (selectionRecord.ref === undefined || selectionRecord.ref === '') {
        // no direct reference given, so create a new selection record
        let newSelectionRef = uuidv4();
        let newFields = 'ref';
        let newValues = '\'' + newSelectionRef + '\'';
        // fields are context, context_type, field, field_type, field_name
        if (selectionRecord.context != undefined && selectionRecord.context != null && selectionRecord.context != '') {
            newFields += ',context';
            newValues += ', \'' + selectionRecord.context + '\'';
        }
        if (selectionRecord.context_type != undefined && selectionRecord.context_type != null && selectionRecord.context_type != '') {
            newFields += ',context_type';
            newValues += ', \'' + selectionRecord.context_type + '\'';
        }
        if (selectionRecord.field != undefined && selectionRecord.field != null && selectionRecord.field != '') {
            newFields += ',field';
            newValues += ', \'' + selectionRecord.field + '\'';
        }
        if (selectionRecord.field_type != undefined && selectionRecord.field_type != null && selectionRecord.field_type != '') {
            newFields += ',field_type';
            newValues += ', \'' + selectionRecord.field_type + '\'';
        }
        if (selectionRecord.field_name != undefined && selectionRecord.field_name != null && selectionRecord.field_name != '') {
            newFields += ',field_name';
            newValues += ', \'' + selectionRecord.field_name + '\'';
        }
        newSelectionQuery = 'INSERT INTO selection (' + newFields + ') VALUES ('
            + newValues + ');'
        pino.info('new selection query: %s', newSelectionQuery);
        pgClient.query(newSelectionQuery, function(err) {
            if (err) {
                pino.error('error recording new selection: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new selection recorded');
            callback({result: 'done', reason: 'selection-posted', ref: newSelectionRef});
        });
    }
    else {
        // use given selection record reference to update record
        // !!! always updating ref is a hack to get commas right
        let updateQuery = 'UPDATE selection SET ref = \'' + selectionRecord.ref + '\'';
        if (selectionRecord.context != undefined && selectionRecord.context != '') {
            updateQuery += ', context = \'' + selectionRecord.context + '\'';
        }
        if (selectionRecord.context_type != undefined && selectionRecord.context_type != '') {
            updateQuery += ', context_type = \'' + selectionRecord.context_type + '\'';
        }
        if (selectionRecord.field != undefined && selectionRecord.field != '') {
            updateQuery += ', field = \'' + selectionRecord.field + '\'';
        }
        if (selectionRecord.field_type != undefined && selectionRecord.field_type != '') {
            updateQuery += ', field_type = \'' + selectionRecord.field_type + '\'';
        }
        if (selectionRecord.field_name != undefined && selectionRecord.field_name != '') {
            updateQuery += ', field_name = \'' + selectionRecord.field_name + '\'';
        }
        updateQuery += ' WHERE ref = \'' + selectionRecord.ref + '\';';
        pino.info('selection update query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating selection: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('selection updated');
            callback({result: 'done', reason: 'selection-posted'});
        });
    }
}

// selectionFetch gets and returns data associated with a selection process
function selectionFetch(pgClient, selectionRef, callback) {
    let fetchQuery = 'SELECT context,context_type,field,field_type,field_name '
        + 'FROM selection WHERE ref=\'' + selectionRef + '\';';
    pino.info('selection fetch query: %s', fetchQuery)
    pgClient.query(fetchQuery, function(err, result) {
        if (err) {
            pino.error('error fetching selection data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('selection fetch result count: %d, row zero: %s', result.rowCount, JSON.stringify(result.rows[0]));
        if (result.rowCount === 0) {
            callback({result: 'failed', reason: 'selection-invalid'});
            return;
        }
        callback({result: 'okay', reason: 'selection-read', 
            context: result.rows[0].context,
            context_type: result.rows[0].context_type,
            field: result.rows[0].field,
            field_type: result.rows[0].field_type,
            field_name: result.rows[0].field_name
        });
    });
}

// selectionDelete removes selection process data upon completion
function selectionDelete(pgClient, selectionRef, callback) {
    let deletionQuery = 'DELETE FROM selection WHERE ref=\''
        + selectionRef + '\';';
    pino.info('delete selection query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
        }
        pino.info('deleted selection');
        callback({result: 'done', reason: 'selection-deleted'});
    });
}

// selection object to be exported by module
var selection = {};
selection.post = selectionPost;
selection.get = selectionFetch;
selection.delete = selectionDelete;
module.exports = selection;

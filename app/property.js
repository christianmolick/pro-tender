// property.js
// basic support for fetching server operating parameters
// !!! note limited functionality:
//     the vision was to have settings for various options
//     but now only name, version, and description are used
//     and these are retained in the package data
//     instead of a database

// propertyFetch function returns named values from table
// propertyName is the name of the property to be fetched
// callback receives the result or nothing if none
// nothing logged, errors return nothing, so use with caution
function propertyFetch(pgClient, propertyName, callback) {
    if (propertyName === undefined || propertyName === '' ) {
        callback();
    }
    if (propertyName != 'header-text' && propertyName != 'header-json') {
        let queryString = 'SELECT value FROM property WHERE name=\'' + propertyName + '\';';
        pgClient.query(queryString, function (err, result) {
            if (err) {
                callback();            
            }
            callback(result.rows[0].value);
        });
    }
    if (propertyName == 'header-text') {
        // !!! this does not work because the order may vary
        let packageObject = require('../package');
        let resultString = '{'
            + '\'' + 'name' + '\': \'' + packageObject.name + '\'' + ', '
            + '\'' + 'version' + '\': \'' + packageObject.version + '\'' + ', '
            + '\'' + 'description' + '\': \'' + packageObject.description + '\'' + '}';
        callback(resultString);
    }
    else if (propertyName == 'header-json') {
        let packageObject = require('../package');
        let resultObject = {
            'name': packageObject.name,
            'version': packageObject.version,
            'description': packageObject.description
        };
        callback(resultObject);
    }
}

// permit object to be exported by module
var property = {};  
property.get = propertyFetch;
module.exports = property;


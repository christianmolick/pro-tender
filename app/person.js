// person.js 
// basic create, read, update, delete functions
// and business logic relating to person records
// intended to provide for robust abstraction
// assumes permit already checked, operates directly
// parameter sanity and range checks also assumed
//
// semantics of person records should be fully documented
// but here briefly noted:
// there may be multiple records describing the same person
// account name reference is important for accountability
// thus a person is uniquely identified by ref
// or name and full name and account name
//
// summary: name, full_name, profile
//
// person records as of 2020-12-20
//    id SERIAL PRIMARY KEY NOT NULL,
//    ref UUID,
//    container UUID,
//    name TEXT,
//    full_name TEXT,
//    language TEXT,    
//    account UUID,
//    place UUID,
//    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    profile TEXT,
//    preferences TEXT,
//    provides TEXT

// use uuidv4 for new record references
//let uuidv4 = require('uuid/v4');
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// personPost is used to create or update person records
// pgClient is for database access
// personRecord is basic json for database
// callback gets result
function personPost(pgClient, personRecord, callback) {
    // debug output
    pino.info('person post');
    // if ref set then presence known
    if (personRecord.ref === undefined || personRecord.ref === '') {
        // no reference means creating a new record
        // starting with a new reference id
        let personRef = uuidv4();
        let newPersonFields = 'ref,name,language';
        let newPersonValues = '\'' + personRef + '\', \''
            + personRecord.name + '\', \''
            + personRecord.language + '\'';
        if (personRecord.full_name !== undefined && personRecord.full_name !== '') {
            newPersonFields = newPersonFields + ',full_name';
            newPersonValues = newPersonValues + ', \'' + personRecord.full_name + '\'';
        }
        if (personRecord.container !== undefined && personRecord.container !== '') {
            newPersonFields = newPersonFields + ',container';
            newPersonValues = newPersonValues + ', \'' + personRecord.container + '\'';
        }
        if (personRecord.account !== undefined && personRecord.account !== '') {
            newPersonFields = newPersonFields + ',account';
            newPersonValues = newPersonValues + ', \'' + personRecord.account + '\'';
        }
        if (personRecord.place !== undefined && personRecord.place !== '') {
            newPersonFields = newPersonFields + ',place';
            newPersonValues = newPersonValues + ', \'' + personRecord.place + '\'';
        }
        if (personRecord.profile !== undefined && personRecord.profile !== '') {
            newPersonFields = newPersonFields + ',profile';
            newPersonValues = newPersonValues + ', \'' + personRecord.profile + '\'';
        }
        if (personRecord.preferences !== undefined && personRecord.preferences !== '') {
            newPersonFields = newPersonFields + ',preferences';
            newPersonValues = newPersonValues + ', \'' + personRecord.preferences + '\'';
        }
        if (personRecord.provides !== undefined && personRecord.provides !== '') {
            newPersonFields = newPersonFields + ',provides';
            newPersonValues = newPersonValues + ', \'' + personRecord.provides + '\'';
        }
        let newPersonQuery = 'INSERT INTO person (' + newPersonFields
            + ') VALUES (' + newPersonValues + ');';
        pino.info('new person query %s', newPersonQuery);
        pgClient.query(newPersonQuery, function(err) {
            if (err) {
                pino.error('error recording new person: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new person recorded');
            callback({result: 'done', reason: 'person-posted', ref: personRef});
            return;
        });
    }
    else {
        // reference present means updating a record
        var updateQuery = 'UPDATE person SET modified = CURRENT_TIMESTAMP ';
        if (personRecord.name != undefined && personRecord.name != '') {
           updateQuery = updateQuery + ', name = \'' + personRecord.name + '\' ';
        }
        if (personRecord.full_name != undefined && personRecord.full_name != '') {
            updateQuery = updateQuery + ', full_name = \'' + personRecord.full_name + '\' ';
        }
        if (personRecord.account != undefined && personRecord.account != '') {
            updateQuery = updateQuery + ', account = \'' + personRecord.account + '\' ';
        }
        if (personRecord.container != undefined && personRecord.container != '') {
            updateQuery = updateQuery + ', container = \'' + personRecord.container + '\' ';
        }
        if (personRecord.place != undefined && personRecord.place != '') {
            updateQuery = updateQuery + ', place = \'' + personRecord.place + '\' ';
        }
        if (personRecord.language != undefined && personRecord.language != '') {
            updateQuery = updateQuery + ', language = \'' + personRecord.language + '\' ';
        }
        if (personRecord.profile != undefined && personRecord.profile != '') {
            updateQuery = updateQuery + ', profile = \'' + personRecord.profile + '\' ';
        }
        if (personRecord.preferences != undefined && personRecord.preferences != '') {
            updateQuery = updateQuery + ', preferences = \'' + personRecord.preferences + '\' ';
        }
        if (personRecord.provides != undefined && personRecord.provides != '') {
            updateQuery = updateQuery + ', provides = \'' + personRecord.provides + '\' ';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + personRecord.ref + '\';';
        pino.info('update person query %s', JSON.stringify(updateQuery));
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating person: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('person record updated');
            callback({result: 'done', reason: 'person-posted', ref: personRecord.ref});
            return;
        });
    }
}

// personFetch is used to get person records
// pgClient is database
// personReference targets record to fetch
// callback gets result
// initial implementation uses only uuid reference or perhaps name or account ref
// but intention is to allow name, full_name, other such
function personFetch(pgClient, personReference, callback) {
    // get person record
    // strictly speaking this logic could leave an empty and invalid qualifier
    // !!! ref not null is a hack, should compose query correctly without that
    // !!! hacking of hack, using account ref instead, not correct?
    var queryQualifiers = 'account=\'' + personReference.account + '\'';
    if (personReference.ref != undefined && personReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' 
        + personReference.ref + '\'';
    }
    if (personReference.name != undefined && personReference.name != '') {
        queryQualifiers = queryQualifiers + ' AND name=\'' 
            + personReference.name + '\'';
    }
    if (personReference.full_name != undefined && personReference.full_name != '') {
        queryQualifiers = queryQualifiers + ' AND full_name=\'' 
            + personReference.full_name + '\'';
    }
    if (personReference.language != undefined && personReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' 
            + personReference.language + '\'';
    }
    if (personReference.profile != undefined && personReference.profile != '') {
        queryQualifiers = queryQualifiers + ' AND profile=\'' 
            + personReference.language + '\'';
    }
    if (personReference.preferences != undefined && personReference.preferences != '') {
        queryQualifiers = queryQualifiers + ' AND preferences=\'' 
            + personReference.preferences + '\'';
    }
    if (personReference.provides != undefined && personReference.provides != '') {
        queryQualifiers = queryQualifiers + ' AND provides=\'' 
            + personReference.provides + '\'';
    }
    // other qualifiers: account, name, full_name
    // !!! person query should also update viewed field
    let personQuery = 
        'UPDATE person SET viewed = CURRENT_TIMESTAMP WHERE '
        + queryQualifiers + ';'
        + 'SELECT ref,container,name,full_name,language,'
        + 'created,viewed,modified,'
        + 'account,place,'
        + 'profile,preferences,provides '
        + 'FROM person WHERE '
        + queryQualifiers + ';';
    pino.info('get person data query: %s', personQuery);
    pgClient.query(personQuery, function(err,result) {
        if (err) {
            pino.error('error reading person data: %s', err);
            callback({result: 'failed', reason: 'error', count: 0});
            return;
        }
        pino.info('person count: %d, data: %s', 
            result[1].rowCount, JSON.stringify(result[1].rows));
        if (result[1].rowCount === 0) {
            // return no such found
            pino.info('no such person found');
            callback({result: 'failed', reason: 'invalid-person', count: 0});
            return;
        }
        else {
            // return one or more results
            pino.info('found, returning data');
            // this is okay instead of done because nothing was changed
            callback({result: 'okay', reason: 'person-read', 
                count: result[1].rowCount, data: result[1].rows});
            return;
        }
    });
}

// personSummary takes a person reference
// and generates and returns a short string representation
function personSummary(accountRef, permitRef, personRef, callback) {
    pino.info('person summary for ref=%s', personRef);
    if (personRef === undefined || personRef === '') {
        callback('');
        return;
    }
    // fetch data for place selected for inspection
    let personFetchArgs = {
        data: {
            account: accountRef,
            permit: permitRef,
            ref: personRef
        },
        headers: {'Content-Type': 'application/json'}
    };
    pino.info('arguments for person fetch: %s', JSON.stringify(personFetchArgs));
    service.get(config.proTender + '/person', personFetchArgs, function(data) {
        pino.info('person fetch data: %s', JSON.stringify(data));
        var summary = data.data[0].name;
        if (data.data[0].full_name != undefined && data.data[0].full_name != '') {
            summary += ', ' + data.data[0].full_name;
        }
        if (data.data[0].type != undefined && data.data[0].type != '') {
            summary += ', ' + data.data[0].type;
        }
        pino.info('person summary: %s', summary);
        // !!! returns empty string if problems with lookup, should be "empty" instead?
        callback(summary);
    });
}

// personDelete removes person records
// pgClient is database
// personReference targets record to delete
// callback gets result on completion
// as with person fetch reference should be generalized but now only uuid
function personDelete(pgClient, personReference, callback) {
    let queryQualifiers = '';
    if (personReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + personReference.ref +'\'';
    }
    let deletionQuery = 'DELETE FROM person '
        + queryQualifiers + ';';
    pino.info('delete person query:', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting person data: %s', err);
            callback({result: 'failed', reason: 'error'});
        }
        pino.info('deleted person data');
        callback({result: 'done', reason: 'person-deleted'});
    });
}

// person object to be exported by module
var person = {};
person.post = personPost;
person.get = personFetch;
person.delete = personDelete;
module.exports = person;

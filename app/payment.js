// payment.js
// routines related to payments
// create, read, update, delete, validate 
//
// this code is so far unused

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// item object to be exported by module
var payment = {};

// paymentValid checks for problems
function paymentValid() {
}

// paymentPost to create or update
// pgClient is database
// itemRecord is basic json for database fields
// callback gets notified of result
function paymentPost(pgClient, paymentRecord, callback) {
    // debug dump
    pino.info('post payment: %s', JSON.stringify(paymentRecord));
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (paymentRecord.ref === undefined || paymentRecord.ref === '') {
        // no unit ref means create new
        let paymentRef = uuidv4();
        let newPaymentQuery = 'INSERT INTO payment (ref, submitter_name, language, topic, description, notes, status) VALUES (\''
            + paymentRef + '\', \'' 
            + paymentRecord.submitter_name + '\', \'' 
            + paymentRecord.language + '\',\''
            + paymentRecord.topic + '\',\'' 
            + paymentRecord.description + '\',\''
            + paymentRecord.notes + '\',\''
            + paymentRecord.status + '\');';
        pino.info('new payment insertion query: %s', newPaymentQuery);
        pgClient.query(newPaymentQuery, function(err) {
            if (err) {
                pino.error('error inserting payment data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new payment inserted');
            callback({result: 'done', reason: 'payment-posted', ref: paymentRef});
            return;
        });  // end insert payment data block
    }
    else {
        // nonempty reference present means update that record
        var updateQuery = 'UPDATE payment SET ';
        var separatorString = '';
        if (paymentRecord.language != undefined && paymentRecord.language != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'language = \'' + paymentRecord.language + '\'';
        }
        if (paymentRecord.topic != undefined && paymentRecord.topic != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'topic = \'' + paymentRecord.topic + '\'';
        }
        if (paymentRecord.description != undefined && paymentRecord.description != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'description = \'' + paymentRecord.description + '\'';
        }
        if (paymentRecord.status != undefined && paymentRecord.status != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'status = \'' + paymentRecord.status + '\'';
        }
        if (paymentRecord.notes != undefined && paymentRecord.notes != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'notes = \'' + paymentRecord.notes + '\'';
        }
        if (paymentRecord.submitter_name != undefined && paymentRecord.submitter_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'submitter_name = \'' + paymentRecord.submitter_name + '\'';
        }
        if (paymentRecord.owner_name != undefined && paymentRecord.owner_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'owner_name = \'' + paymentRecord.owner_name + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + paymentRecord.ref + '\';';
        pino.info('update payment query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating payment: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new payment added');
            callback({result: 'done', reason: 'payment-posted', ref: paymentRecord.ref});
            return;
        });
    }
}

function paymentFetch(pgClient, paymentReference, callback) {
    pino.info('get payment ref = %s', JSON.stringify(paymentReference));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // get payment record
    var queryQualifiers = 'submitter_name=\'' + paymentReference.account + '\'';
    if (paymentReference.language != undefined && paymentReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' + paymentReference.language + '\'';
    }
    if (paymentReference.ref != undefined && paymentReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + paymentReference.ref + '\'';
    }
    if (paymentReference.topic != undefined && paymentReference.topic != '') {
        queryQualifiers = queryQualifiers + ' AND topic=\'' + paymentReference.topic + '\'';
    }
    if (paymentReference.description != undefined && paymentReference.description != '') {
        queryQualifiers = queryQualifiers + ' AND description=\'' + paymentReference.description + '\'';
    }
    if (paymentReference.status != undefined && paymentReference.status != '') {
        queryQualifiers = queryQualifiers + ' AND status=\'' + paymentReference.status + '\'';
    }
    let paymentQuery = 'SELECT ref,topic,description,status FROM payment WHERE '
        + queryQualifiers + ';';
    pino.info('get payment query: %s', paymentQuery);
    pgClient.query(paymentQuery, function(err, result) {
         if (err) {
            pino.error('error reading payment data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('payment count: %d, data: %s', result.rowCount, JSON.stringify(result.rows));
        callback({result: 'done', reason: 'payment-read',
            count: result.rowCount, data: result.rows
        });
        return;
    });  // end payment data query block
}

function paymentDelete(pgClient, paymentReference, callback) {
    pino.info('payment delete, ref=%s', paymentReference.ref);
    // delete payment record
    var queryQualifiers = '';
    if (paymentReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + paymentReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM payment '
        + queryQualifiers + ';';
    pino.info('delete payment query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting payment data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        callback({result: 'done', reason: 'payment-deleted'});
        return;
    });  // end deletion query block
}

payment.post = paymentPost;
payment.get = paymentFetch;
payment.delete = paymentDelete;
module.exports = payment;

// issue.js
// basic create, read, update, delete functions
// and business logic relating to issue records
// intended to provide for robust abstraction
//
// semantics ... may be multiple records for the same item
// may be restrictions on access, account name reference for accountability
// !!! so far this is initial sketch that is not used

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// issueValid checks for problems
function issueValid() {
}

// issuePost to create or update
// pgClient is database
// itemRecord is basic json for database fields
// callback gets notified of result
function issuePost(pgClient, issueRecord, callback) {
    // debug dump
    pino.info('post issue: %s', JSON.stringify(issueRecord));
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (issueRecord.ref === undefined || issueRecord.ref === '') {
        // no unit ref means create new
        let issueRef = uuidv4();
        let newIssueQuery = 'INSERT INTO issue (ref, language, topic, summary, notes, status) VALUES (\''
            + issueRef + '\', \'' 
            + issueRecord.language + '\',\''
            + issueRecord.topic + '\',\'' 
            + issueRecord.summary + '\',\''
            + issueRecord.notes + '\',\''
            + issueRecord.status + '\');';
        pino.info('new issue insertion query: %s', newIssueQuery);
        pgClient.query(newIssueQuery, function(err) {
            if (err) {
                pino.error('error inserting issue data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new issue inserted');
            callback({result: 'done', reason: 'issue-posted', ref: issueRef});
            return;
        });  // end insert issue data block
    }
    else {
        // nonempty reference present means update that record
        var updateQuery = 'UPDATE issue SET ';
        var separatorString = '';
        if (issueRecord.language != undefined && issueRecord.language != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'language = \'' + issueRecord.language + '\'';
        }
        if (issueRecord.topic != undefined && issueRecord.topic != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'topic = \'' + issueRecord.topic + '\'';
        }
        if (issueRecord.description != undefined && issueRecord.description != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'description = \'' + issueRecord.description + '\'';
        }
        if (issueRecord.status != undefined && issueRecord.status != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'status = \'' + issueRecord.status + '\'';
        }
        if (issueRecord.notes != undefined && issueRecord.notes != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'notes = \'' + issueRecord.notes + '\'';
        }
        if (issueRecord.submitter_name != undefined && issueRecord.submitter_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'submitter_name = \'' + issueRecord.submitter_name + '\'';
        }
        if (issueRecord.owner_name != undefined && issueRecord.owner_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'owner_name = \'' + issueRecord.owner_name + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + issueRecord.ref + '\';';
        pino.info('update issue query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating issue: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new issue added');
            callback({result: 'done', reason: 'issue-posted', ref: issueRecord.ref});
            return;
        });
    }
}

function issueFetch(pgClient, issueReference, callback) {
    pino.info('get issue ref = %s', JSON.stringify(issueReference));
    // parameters in req.body: session_tag, account_name,
    // and optionally name and/or full_name
    // get issue record
    var queryQualifiers = 'submitter_name=\'' + issueReference.account_name + '\'';
    if (issueReference.language != undefined && issueReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' + issueReference.language + '\'';
    }
    if (issueReference.ref != undefined && issueReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + issueReference.ref + '\'';
    }
    if (issueReference.summary != undefined && issueReference.summary != '') {
        queryQualifiers = queryQualifiers + ' AND summary=\'' + issueReference.summary + '\'';
    }
    if (issueReference.details != undefined && issueReference.details != '') {
        queryQualifiers = queryQualifiers + ' AND details=\'' + issueReference.details + '\'';
    }
    if (issueReference.status != undefined && issueReference.status != '') {
        queryQualifiers = queryQualifiers + ' AND status=\'' + issueReference.status + '\'';
    }
    let issueQuery = 'SELECT ref,summary,details,status FROM issue WHERE '
        + queryQualifiers + ';';
    pino.info('get issue query: %s', issueQuery);
    pgClient.query(issueQuery, function(err, result) {
         if (err) {
            pino.error('error reading issue data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('issue count: %d, data: %s', result.rowCount, JSON.stringify(result.rows));
        callback({result: 'done', reason: 'issue-read',
            count: result.rowCount, data: result.rows
        });
        return;
    });  // end issue data query block
}

function issueDelete(pgClient, issueReference, callback) {
    pino.info('issue delete, ref=%s', issueReference.ref);
    // delete place record
    var queryQualifiers = '';
    if (issueReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + issueReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM issue '
        + queryQualifiers + ';';
    pino.info('delete issue query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting issue data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        //pino.info('place data:',result);
        callback({result: 'done', reason: 'issue-deleted'});
        return;
    });  // end issue deletion query block
}

// issue object to be exported by module
var issue = {};
issue.post = issuePost;
issue.get = issueFetch;
issue.delete = issueDelete;
module.exports = issue;

// request.js
// basic create, read, update, delete functions
// and business logic relating to request records
// intended to provide for robust abstraction
//
// record fields:
//    id SERIAL PRIMARY KEY NOT NULL,
//    ref UUID,
//    language TEXT,
//    summary TEXT,
//    details TEXT,
//    location TEXT,
//    status TEXT,
//    submitter TEXT,
//    provider TEXT,
//    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
//    expected TIMESTAMP WITH TIME ZONE,
//    started TIMESTAMP WITH TIME ZONE,
//    completed TIMESTAMP WITH TIME ZONE,
//    reviewed TIMESTAMP WITH TIME ZONE,
//    paid TIMESTAMP WITH TIME ZONE
//
// * briefly:
// id, ref, lang, summ, detl, loc, status, submitter, provider
// created, viewed, modified, expected, started, completed, reviewed, paid
// * semantics ... 
// !!! so far this is initial sketch that is not used
//     requests have been replaced by events

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// requestValid checks for problems
function requestValid() {
}

// requestPost to create or update
// pgClient is database
// requestRecord is basic json for database fields
// callback gets notified of result
function requestPost(pgClient, requestRecord, callback) {
    pino.info('post request: %s', JSON.stringify(requestRecord));
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (requestRecord.ref === undefined || requestRecord.ref === '') {
        // no unit ref means create new
        let requestRef = uuidv4();
        let newRequestQuery = 'INSERT INTO request (ref, language, summary, details, location, status, submitter, provider) VALUES (\''
            + requestRef + '\', \'' 
            + requestRecord.language + '\',\''
            + requestRecord.summary + '\',\''
            + requestRecord.details + '\',\''
            + requestRecord.location + '\',\''
            + requestRecord.status + '\',\''
            + requestRecord.submitter + '\', \'' 
            + requestRecord.provider + '\');';
            // expected omitted because undefined not allowed
        pino.info('new request insertion query: %s', newRequestQuery);
        pgClient.query(newRequestQuery, function(err) {
            if (err) {
                pino.error('error inserting request data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new request inserted');
            callback({result: 'done', reason: 'request-posted', ref: requestRef});
            return;
        });  // end insert request data block
    }
    else {
        // nonempty reference present means update that record
// id, ref, lang, summ, detl, loc, status, submitter, provider
// created, viewed, modified, expected, started, completed, reviewed, paid
        var updateQuery = 'UPDATE request SET ';
        var separatorString = '';
        if (requestRecord.language != undefined && requestRecord.language != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'language = \'' + requestRecord.language + '\'';
        }
        if (requestRecord.summary != undefined && requestRecord.summary != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'summary = \'' + requestRecord.summary + '\'';
        }
        if (requestRecord.details != undefined && requestRecord.details != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'details = \'' + requestRecord.details + '\'';
        }
        if (requestRecord.location != undefined && requestRecord.location != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'location = \'' + requestRecord.location + '\'';
        }
        if (requestRecord.status != undefined && requestRecord.status != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'status = \'' + requestRecord.status + '\'';
        }
        if (requestRecord.submitter != undefined && requestRecord.submitter != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'submitter = \'' + requestRecord.submitter + '\'';
        }
        if (requestRecord.provider != undefined && requestRecord.provider != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'provider = \'' + requestRecord.provider + '\'';
        }
        // expected is a timestamp with time zone, so just a bit different
        if (requestRecord.expected != undefined) {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'expected = \'' + requestRecord.expected + '\'';
        }
        updateQuery = updateQuery + ' WHERE ref = \'' + requestRecord.ref + '\';';
        pino.info('update request query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating request: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new request added');
            callback({result: 'done', reason: 'request-posted', ref: requestRecord.ref});
            return;
        });
    }
}

function requestFetch(pgClient, requestReference, callback) {
    pino.info('get request ref = %s', JSON.stringify(requestReference));
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // get request record
    var queryQualifiers = 'submitter_name=\'' + requestReference.account + '\'';
    if (requestReference.ref != undefined && requestReference.ref != '') {
        queryQualifiers = queryQualifiers + ' AND ref=\'' + requestReference.ref + '\'';
    }
    if (requestReference.language != undefined && requestReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' + requestReference.language + '\'';
    }
    if (requestReference.summary != undefined && requestReference.summary != '') {
        queryQualifiers = queryQualifiers + ' AND summary=\'' + requestReference.summary + '\'';
    }
    if (requestReference.details != undefined && requestReference.details != '') {
        queryQualifiers = queryQualifiers + ' AND details=\'' + requestReference.details + '\'';
    }
    if (requestReference.location != undefined && requestReference.location != '') {
        queryQualifiers = queryQualifiers + ' AND location=\'' + requestReference.location + '\'';
    }
    if (requestReference.status != undefined && requestReference.status != '') {
        queryQualifiers = queryQualifiers + ' AND status=\'' + requestReference.status + '\'';
    }
    if (requestReference.submitter != undefined && requestReference.submitter != '') {
        queryQualifiers = queryQualifiers + ' AND submitter=\'' + requestReference.submitter + '\'';
    }
    if (requestReference.provider != undefined && requestReference.provider != '') {
        queryQualifiers = queryQualifiers + ' AND provider=\'' + requestReference.provider + '\'';
    }
    // expected is a timestamp with time zone, so some differences
    if (requestReference.expected != undefined) {
        queryQualifiers = queryQualifiers + ' AND expected=\'' + requestReference.expected + '\'';
    }
    // id, ref, lang, summ, detl, loc, status, submitter, provider
    // created, viewed, modified, expected, started, completed, reviewed, paid
    let requestQuery = 'SELECT '
        + 'ref,language,summary,details,location,status,submitter,provider '
        + 'FROM request '
        + 'WHERE ' + queryQualifiers + ';';
    pino.info('get request query string: %s', requestQuery);
    pgClient.query(requestQuery, function(err, result) {
         if (err) {
            pino.error('error reading request data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('request count: %d, data: %s', result.rowCount, JSON.stringify(result.rows));
        callback({result: 'done', reason: 'request-read',
            count: result.rowCount, data: result.rows
        });
        return;
    });  // end request data query block
}

function requestDelete(pgClient, requestReference, callback) {
    pino.info('request delete, ref=%s', requestReference.ref);
    // delete request record
    var queryQualifiers = '';
    if (requestReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + requestReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM request '
        + queryQualifiers + ';';
    pino.info('delete request query: %s', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting request data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        //pino.info('request data:',result);
        callback({result: 'done', reason: 'request-deleted'});
        return;
    });  // end request deletion query block
}

// item object to be exported by module
var request = {};
request.post = requestPost;
request.get = requestFetch;
request.delete = requestDelete;
module.exports = request;

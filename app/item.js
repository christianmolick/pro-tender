// item.js
// basic create, read, update, delete functions
// and business logic relating to item records
// intended to provide for robust abstraction
//
// semantics ... may be multiple records for the same item
// may be restrictions on access, account name reference for accountability
// !!! so far this is initial sketch that is not used

// uuidv4 for new record references
let { v4: uuidv4 } = require('uuid');

// log activity with pino
let pino = require('pino')();

// itemValid checks for problems
function itemValid() {
}

// itemPost to create or update
// pgClient is database
// itemRecord is basic json for database fields
// callback gets notified of result
function itemPost(pgClient, itemRecord, callback) {
    // debug dump
    pino.info('post item');
    // sanity/validity check
    // conflict/context check
    // presence of ref means update existing otherwise create new
    if (itemRecord.ref === undefined || itemRecord.ref === '') {
        // no unit ref means create new
        let itemRef = uuidv4();
        let newItemQuery = 'INSERT INTO item (ref,account, name, full_name, language, type, description) VALUES (\''
            + itemRef + '\', \'' 
            + itemRecord.account + '\', \'' 
            + itemRecord.name + '\',\'' 
            + itemRecord.full_name + '\',\'' 
            + itemRecord.language + '\',\''
            + itemRecord.type + '\',\'' 
            + itemRecord.description + '\');';
        pino.info('new item insertion query: %s', newItemQuery);
        pgClient.query(newItemQuery, function(err) {
            if (err) {
                pino.error('error inserting item data: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new item inserted');
            callback({result: 'done', reason: 'item-posted', ref: itemRef});
            return;
        });  // end insert item data block
    }
    else {
        // nonempty reference present means update that record
        var updateQuery = 'UPDATE item SET ';
        var separatorString = '';
        if (itemRecord.name != undefined && itemRecord.name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'name = \'' + itemRecord.name + '\'';
        }
          if (itemRecord.full_name != undefined && itemRecord.full_name != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'full_name = \'' + itemRecord.full_name + '\'';
        }
          if (itemRecord.accout != undefined && itemRecord.account != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'account = \'' + itemRecord.account + '\'';
        }
          if (itemRecord.language != undefined && itemRecord.language != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'language = \'' + itemRecord.language + '\'';
        }
          if (itemRecord.type != undefined && itemRecord.type != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'type = \'' + itemRecord.type + '\'';
        }
          if (itemRecord.description != undefined && itemRecord.description != '') {
            if (separatorString === '') {
                separatorString = ', ';
            }
            else {
                updateQuery = updateQuery + separatorString;
            }
            updateQuery = updateQuery + 'description = \'' + itemRecord.description;
        }
        // notes?
        updateQuery = updateQuery + ' WHERE ref = \'' + itemRecord.ref + '\';';
        pino.info('update item query: %s', updateQuery);
        pgClient.query(updateQuery, function(err) {
            if (err) {
                pino.error('error updating item: %s', err);
                callback({result: 'failed', reason: 'error'});
                return;
            }
            pino.info('new item added');
            callback({result: 'done', reason: 'item-posted', ref: itemRecord.ref});
            return;
        });
    }
}

function itemFetch(pgClient, itemReference, callback) {
    pino.info('get item');
    // parameters in req.body: session_tag, account,
    // and optionally name and/or full_name
    // get item record
    var queryQualifiers = 'account=\'' + itemReference.account + '\'';
    if (itemReference.name != undefined && itemReference.name != '') {
        queryQualifiers = queryQualifiers + ' AND name=\'' + itemReference.name + '\'';
    }
    if (itemReference.full_name != undefined && itemReference.full_name != '') {
        queryQualifiers = queryQualifiers + ' AND full_name=\'' + itemReference.full_name + '\'';
    }
    if (itemReference.language != undefined && itemReference.language != '') {
        queryQualifiers = queryQualifiers + ' AND language=\'' + itemReference.language + '\'';
    }
    if (itemReference.type != undefined && itemReference.type != '') {
        queryQualifiers = queryQualifiers + ' AND type=\'' + itemReference.type + '\'';
    }
    if (itemReference.description != undefined && itemReference.description != '') {
        queryQualifiers = queryQualifiers + ' AND description=\'' + itemReference.description + '\'';
    }
    let itemQuery = 'SELECT ref,name,full_name,type FROM item WHERE '
        + queryQualifiers + ';';
    pino.info('get item query: %s', itemQuery);
    pgClient.query(itemQuery, function(err, result) {
         if (err) {
            pino.error('error reading item data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        pino.info('item data: %s', JSON.stringify(result.rows));
        callback({result: 'done', reason: 'item-read',
            count: result.rowCount, data: result.rows
        });
        return;
    });  // end item data query block
}

function itemDelete(pgClient, itemReference, callback) {
    pino.info('item delete');
    // delete item record
    var queryQualifiers = '';
    if (itemReference.ref) {
        queryQualifiers = 'WHERE ref=\'' + itemReference.ref + '\'';
    }
    let deletionQuery = 'DELETE FROM item '
        + queryQualifiers + ';';
    pino.info('delete item query:', deletionQuery);
    pgClient.query(deletionQuery, function(err) {
        if (err) {
            pino.error('error deleting item data: %s', err);
            callback({result: 'failed', reason: 'error'});
            return;
        }
        //pino.info('item data:',result);
        callback({result: 'done', reason: 'item-deleted'});
        return;
    });  // end item deletion query block
}

// item object to be exported by module
var item = {};
item.post = itemPost;
item.get = itemFetch;
item.delete = itemDelete;
module.exports = item;

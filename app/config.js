// pro service configuration
var config = {};

// db
config.dbAccessString = 'postgres://tender:tdr@localhost:5432/pro';

// port for service
config.servicePort = '51231';

// account service port
config.accountTenderPort = '51112';

// hand off data as required
module.exports = config;

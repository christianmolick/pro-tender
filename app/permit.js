// permit.js
// business logic for handling of permits
// how to receive them, check them,
// and what to do if one bites you
let clientSource = require('node-rest-client').Client;
let config = require('./config');

// permitCheck function gets lookup from account tender
// permitUnchecked is the permit to be checked
// accountName is the asssociated account name
// accountTenderPort is the address of the account service
// callback gets the true or false result
function permitCheck(account, permitUnchecked, usage, callback) {
    let permitCheckArgs = {
        data: {
            permit: permitUnchecked,
            account: account,
            usage: usage
        },
        headers: {'Content-Type': 'application/json'}
    };
    let client = new clientSource();
    client.get('http://localhost:' + config.accountTenderPort + '/permit/valid', permitCheckArgs, function(data, response) {
        // 200 ok, 201 created, 202 accepted
        // 100s info, 300s redirect, 400s errors, 500s server errors
        if (response.statusCode === 404) {
            callback();
        } else {
            callback(data);
        }
    });
}

var permit = {};  // permit object to be exported by module
permit.check = permitCheck;
module.exports = permit;

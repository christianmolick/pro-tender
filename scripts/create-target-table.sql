-- event table tracks things that have happened, are happening, may happen
-- container represents trees of subtasks: housecleaning, kitchen, microwave
-- language is primary language used for a record
-- summary is most important basic description of service
-- details is for all manner of notes and qualifications
-- person and place are direct references
-- client and provider are account references
-- process is a reference to a specific process which is not yet supported
-- created, viewed, modified track the history of this record
-- proposed, confirmed, expected, arrived, started, completed, billed, paid, reviewed
-- all record times of specific stages that indirectly reveal event status
DROP TABLE "event";
CREATE TABLE "event" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    container UUID,
    language TEXT,
    summary TEXT,
    details TEXT,
    person UUID,
    place UUID,
    client UUID,
    provider UUID,
    process UUID,
    
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    
    proposed TIMESTAMP WITH TIME ZONE,
    confirmed TIMESTAMP WITH TIME ZONE,
    expected TIMESTAMP WITH TIME ZONE,
    arrived TIMESTAMP WITH TIME ZONE,
    started TIMESTAMP WITH TIME ZONE,
    completed TIMESTAMP WITH TIME ZONE,
    billed TIMESTAMP WITH TIME ZONE,
    paid TIMESTAMP WITH TIME ZONE,
    reviewed TIMESTAMP WITH TIME ZONE
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE event TO tender;
GRANT USAGE, SELECT ON SEQUENCE event_id_seq TO tender;
-- no seed values

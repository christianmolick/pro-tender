# all interactions with postgres are as postgres user
# first drop database to be sure of clean creation
sudo -u postgres dropdb pro
# create database with tender role and utf-8 encoding
sudo -u postgres createdb -O tender --encoding=utf8 pro 'pro tender'
# grant tender role permission to create tables
sudo -u postgres psql -W -d pro -e -f scripts/grant-db-role-privs.sql

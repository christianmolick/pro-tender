-- results describe end state of process, event, request, other
-- !!! initial sketch only so far unused
DROP TABLE "result";
CREATE TABLE "result" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    brief TEXT,
    summary TEXT,
    client TEXT,
    provider TEXT,
    place TEXT,
    target UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    progress TEXT,
    result TEXT,
    start TIMESTAMP WITH TIME ZONE,
    duration INTEGER,
    fee INTEGER
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE result TO tender;
GRANT USAGE, SELECT ON SEQUENCE result_id_seq TO tender;
-- no seed values

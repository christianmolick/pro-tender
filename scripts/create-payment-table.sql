-- ROUGH INITIAL SKETCH ONLY
-- DATA NEEDED FOR PAYMENTS IS NOT YET CLEAR
DROP TABLE "payment";
-- now create the table
CREATE TABLE "payment" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref uuid,
    name TEXT NOT NULL,
    full_name TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    remarks TEXT,
    regarding uuid,
    reviewer TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE payment TO tender;
GRANT USAGE, SELECT ON SEQUENCE payment_id_seq TO tender;

-- places have names, possibly containers, and qualifications
-- not sure about name and full_name
-- container for trees: country, nation, state, county, city, street, address, unit
-- type is region, post code, street, unit, room, area
-- description are details of place
-- language code is for language used in a particular record
DROP TABLE "place";
CREATE TABLE "place" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    container UUID,
    language TEXT,
    name TEXT NOT NULL,
    full_name TEXT,
    account UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE,
    modified TIMESTAMP WITH TIME ZONE,
    type TEXT,
    description TEXT,
    notes TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE place TO tender;
GRANT USAGE, SELECT ON SEQUENCE place_id_seq TO tender;
-- no seed values

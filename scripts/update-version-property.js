// update version entry in property table
// arguably the version should always be read from the package.json file
// but operations are most consistent if all necessary data is in tables

// get database configuration
let config = require('../app/config');

// load database library and connect
let pg = require('pg');
let pgClient = new pg.Client(config.dbAccessString);
pgClient.connect(function (err) {
    if (err) {
        pino.error('account tender could not connect to postgres: %s', err);
    }
});

// fetch current project version from package.json
let current = require('../package').version;

// oopid ahtay
// table initialization for reference:
// INSERT INTO "property" (name, language, value, public)
// VALUES ('version', 'en', '1.1.0', 'true');

//let debugDump = 'SELECT property WHERE name = \'name\' OR name = \'version\';';
//let debugDump = 'SELECT name,language,public,value FROM property;';
//let debugDump = 'SELECT name,language,public,value FROM property '
//    + 'WHERE name=\'name\' OR name=\'version\';';
//let debugDump = 'SELECT property WHERE name = \'name\';';
//console.log('debug dump:', debugDump);
//pgClient.query(debugDump, function(err, result) {
//    console.log('error', JSON.stringify(err));
//    console.log('result', JSON.stringify(result));
//    process.exit();
//});

let updateQuery = 'UPDATE property SET value = \'' + current
    + '\' WHERE name = \'version\';';
console.log('attempting to update version entry in property table to', current);
console.log('update query:', updateQuery);
pgClient.query(updateQuery, function(err, result) {
    if (err) {
	    console.log('error updating version entry in property table', err);
        process.exit();
    }
    console.log('version entry in property table successfully updated');
    process.exit();
});

-- processes have brief and summary description,
-- container represents tree relations: housecleaning, kitchen, microwave
-- client and provider references to person or account records
-- place, start, duration, and fee
-- need expectation, result, previous, next 
-- !!! initial sketch only so far not used
DROP TABLE "process";
CREATE TABLE "process" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    container UUID,
    brief TEXT,
    summary TEXT,
    client UUID,
    provider UUID,
    place UUID,
    target UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE,
    duration INTEGER,
    fee INTEGER
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE process TO tender;
GRANT USAGE, SELECT ON SEQUENCE process_id_seq TO tender;
-- no seed values

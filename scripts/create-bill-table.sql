-- INITIAL SKETCH ONLY
-- DATA NEEDED FOR PAYMENTS IS UNCLEAR 
-- client and provider are account references
-- event is the activity generating the fee
-- due and paid are timestamps recording these actions
-- payable and received are text including currency, example "50 USD"
-- amounts are base fees not including calculations, taxes, other fees
DROP TABLE "bill";
-- now create the table
CREATE TABLE "bill" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    client UUID,
    provider UUID,
    event UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    due TIMESTAMP WITH TIME ZONE,
    paid TIMESTAMP WITH TIME ZONE,
    payable TEXT,
    received TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE bill TO tender;
GRANT USAGE, SELECT ON SEQUENCE bill_id_seq TO tender;

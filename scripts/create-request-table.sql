-- request table tracks requests for service, products, other
-- !!! initial sketch so far unused
-- requests have been replaced by events
DROP TABLE "request";
CREATE TABLE "request" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    language TEXT,
    summary TEXT,
    details TEXT,
    location TEXT,
    submitter TEXT,
    provider TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    expected TIMESTAMP WITH TIME ZONE,
    started TIMESTAMP WITH TIME ZONE,
    completed TIMESTAMP WITH TIME ZONE,
    reviewed TIMESTAMP WITH TIME ZONE,
    paid TIMESTAMP WITH TIME ZONE
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE request TO tender;
GRANT USAGE, SELECT ON SEQUENCE request_id_seq TO tender;
-- no seed values

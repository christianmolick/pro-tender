# create production account database tables
# sudo -u postgres to run psql as postgres user
# -W to force password prompt -- no longer considered best practice
# -U no longer used?
# -d service indicates database
# -e to dump all sql
# -f to load from indicated file
# properties are internal service options
sudo -u postgres psql -d pro -e -f scripts/create-property-table.sql
# targets are a heirarchy of deliverables
sudo -u postgres psql -d pro -e -f scripts/create-target-table.sql
# notes relate to targets for an event, progress, or invoice
sudo -u postgres psql -d pro -e -f scripts/create-notes-table.sql
# events are scheduled actions for an account and target
sudo -u postgres psql -d pro -e -f scripts/create-event-table.sql
# progress tracks events as target is serviced for account
sudo -u postgres psql -d pro -e -f scripts/create-progress-table.sql
# invoices are the results of progress for an event
sudo -u postgres psql -d pro -e -f scripts/create-invoice-table.sql


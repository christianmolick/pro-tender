-- persons have properties for service provision
-- account and place are uuid references
-- container for trees: company, org, family, then member
-- name and full_name should not be null
-- name and possibly language and profile may be take from account
-- but the database should not be enforcing that
DROP TABLE "person";
CREATE TABLE "person" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    container UUID,
    name TEXT,
    full_name TEXT,
    language TEXT,    
    account UUID,
    place UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE,
    modified TIMESTAMP WITH TIME ZONE,
    profile TEXT,
    preferences TEXT,
    provides TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE person TO tender;
GRANT USAGE, SELECT ON SEQUENCE person_id_seq TO tender;
-- no seed values

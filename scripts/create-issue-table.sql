-- issue table tracks ideas, optimizations, pitfalls, other such
-- !!! initial sketch so far not used
DROP TABLE "issue";
CREATE TABLE "issue" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    language TEXT,
    topic TEXT,
    summary TEXT,
    details TEXT,
    comments TEXT,
    priority TEXT,
    notes TEXT,
    environment TEXT,
    related TEXT,
    submitter UUID,
    owner UUID,
    fixer UUID,
    fix_description TEXT,
    fix_package_version TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    fixed TIMESTAMP WITH TIME ZONE,
    submitter_name TEXT,
    owner_name TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE issue TO tender;
GRANT USAGE, SELECT ON SEQUENCE issue_id_seq TO tender;
-- no seed values

-- selection table tracks selection of field values referring to other data
-- possibly also record default, constraints, accompanying message, timestamps
DROP TABLE "selection";
CREATE TABLE "selection" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    context UUID,
    context_type TEXT,
    field UUID,
    field_type TEXT,
    field_name TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE selection TO tender;
GRANT USAGE, SELECT ON SEQUENCE selection_id_seq TO tender;
-- no seed values

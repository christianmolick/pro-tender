-- reviews extend other entities
-- regarding is uuid to target of review
-- review target is almost always a process
-- reviewer person reference
-- unclear if here is best for not null logic constraint
-- !!! initial sketch so far unused
DROP TABLE "review";
CREATE TABLE "review" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    reviewer_account_name TEXT,
    reviewer UUID,
    process UUID,
    target UUID,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    remarks TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE review TO tender;
GRANT USAGE, SELECT ON SEQUENCE review_id_seq TO tender;
-- no seed values

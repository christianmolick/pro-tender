-- items include orders, materials, components, products, other
-- !!! so far initial sketch remaining unused
DROP TABLE "item";
CREATE TABLE "item" (
    id SERIAL PRIMARY KEY NOT NULL,
    ref UUID,
    name TEXT NOT NULL,
    full_name TEXT,
    language TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    viewed TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    account_name TEXT,
    type TEXT,
    description TEXT,
    notes TEXT
);
-- grant full access of table to tender user/role
GRANT ALL PRIVILEGES ON TABLE item TO tender;
GRANT USAGE, SELECT ON SEQUENCE item_id_seq TO tender;
-- no seed values
